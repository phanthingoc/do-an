<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSanPhamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sanpham', function (Blueprint $table) {
            $table->increments('b_id');
            $table->string('b_name');
            $table->string('mota')->nullable();
            $table->string('b_image')->nullable();
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('ca_id')->on('category');
            $table->integer('ncc_id')->unsigned();
            $table->foreign('ncc_id')->references('ncc_id')->on('ncc');
            $table->integer('gia');
            $table->integer('soluong');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sanpham');
    }
}
