<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('category', 'App\Http\Controllers\Admin\CategoryController@index')->name('category.index');
Route::get('category/{id}', 'App\Http\Controllers\Admin\CategoryController@show')->name('category.show');
Route::post('category', 'App\Http\Controllers\Admin\CategoryController@store')->name('category.store');
Route::put('category/{id}', 'App\Http\Controllers\Admin\CategoryController@update')->name('category.update');

Route::get('user', 'App\Http\Controllers\Admin\UserController@index')->name('user.index');
Route::get('user/{id}', 'App\Http\Controllers\Admin\UserController@show')->name('user.show');
Route::post('user', 'App\Http\Controllers\Admin\UserController@store')->name('user.store');
Route::put('user/{id}', 'App\Http\Controllers\Admin\UserController@update')->name('user.update');

Route::get('ncc', 'App\Http\Controllers\Admin\NccController@index')->name('ncc.index');
Route::get('ncc/{id}', 'App\Http\Controllers\Admin\NccController@show')->name('ncc.show');
Route::post('ncc', 'App\Http\Controllers\Admin\NccController@store')->name('ncc.store');
Route::put('ncc/{id}', 'App\Http\Controllers\Admin\NccController@update')->name('ncc.update');

Route::get('sanpham', 'App\Http\Controllers\Admin\SanphamController@index')->name('sanpham.index');
Route::get('sanpham/{id}', 'App\Http\Controllers\Admin\SanphamController@show')->name('sanpham.show');
Route::post('sanpham', 'App\Http\Controllers\Admin\SanphamController@store')->name('sanpham.store');
Route::put('sanpham/{id}', 'App\Http\Controllers\Admin\SanphamController@update')->name('sanpham.update');


