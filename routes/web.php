<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Web\HomeController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=>'admin'], function () {
    Route::group(['middleware' => 'auth'], function () {
        
        Route::get('/home', 'App\Http\Controllers\Admin\AdminController@index')->name('admin.home');

        Route::get('/category', 'App\Http\Controllers\Admin\AdminController@getCategory')->name('admin.category.index');
        

        Route::get('/user', 'App\Http\Controllers\Admin\AdminController@getUser')->name('admin.user.index');
        

        Route::get('/ncc', 'App\Http\Controllers\Admin\AdminController@getNCC')->name('admin.NCC.index');
        

        Route::get('/sanpham', 'App\Http\Controllers\Admin\AdminController@getSanPham')->name('admin.sanpham.index');
        

        Route::get('/order', 'App\Http\Controllers\Admin\AdminController@order')->name('admin.order.index');
        Route::get('/view_order/{id}', 'App\Http\Controllers\Admin\AdminController@view_order')->name('admin.order.view');

        Route::get('/complete/{id}', 'App\Http\Controllers\Admin\AdminController@complete')->name('admin.order.complete');
        Route::get('/thong-ke', 'App\Http\Controllers\Admin\AdminController@thong_ke')->name('admin.thong-ke');
        Route::get('/doanh-thu', 'App\Http\Controllers\Admin\AdminController@doanhThu')->name('admin.doanh-thu');

        Route::group(['middleware' => 'check_admin'], function () {
            Route::get('/category/create', 'App\Http\Controllers\Admin\AdminController@createCategory')->name('admin.category.create');
            Route::get('/category/edit/{id}', 'App\Http\Controllers\Admin\AdminController@updateCategory')->name('admin.category.edit');
            Route::get('/category/delete/{id}', 'App\Http\Controllers\Admin\CategoryController@destroy')->name('admin.category.delete');

            Route::get('/user/create', 'App\Http\Controllers\Admin\AdminController@createUser')->name('admin.user.create');
            Route::get('/user/edit/{id}', 'App\Http\Controllers\Admin\AdminController@updateUser')->name('admin.user.edit');
            Route::get('/user/delete/{id}', 'App\Http\Controllers\Admin\UserController@destroy')->name('admin.user.delete');

            Route::get('/ncc/create', 'App\Http\Controllers\Admin\AdminController@createNCC')->name('admin.NCC.create');
            Route::get('/ncc/edit/{id}', 'App\Http\Controllers\Admin\AdminController@updateNCC')->name('admin.NCC.edit');
            Route::get('/ncc/delete/{id}', 'App\Http\Controllers\Admin\NccController@destroy')->name('admin.NCC.delete');

            Route::get('/sanpham/create', 'App\Http\Controllers\Admin\AdminController@createSanPham')->name('admin.sanpham.create');
            Route::get('/sanpham/edit/{id_b}', 'App\Http\Controllers\Admin\AdminController@updateSanPham')->name('admin.sanpham.edit');
            Route::get('/sanpham/delete/{id_b}', 'App\Http\Controllers\Admin\SanphamController@destroy')->name('admin.sanpham.delete');

        });
    });
    Route::get('/register', [AuthController::class, 'register'])->name('admin.register');
    Route::post('/register', [AuthController::class, 'registerPost'])->name('admin.registerPost');
    Route::get('/login', [AuthController::class, 'login'])->name('admin.login');
    Route::post('/login', [AuthController::class, 'loginPost'])->name('admin.loginPost');
    Route::get('/logout', [AuthController::class, 'logout'])->name('admin.logout');
});



Route::get('/', [\App\Http\Controllers\Web\HomeController::class, 'index'])->name('home');
Route::post('/tim_kiem','App\Http\Controllers\Web\HomeController@search')->name('search');
Route::get('danhmuc/{id}','App\Http\Controllers\Web\HomeController@getDanhmuc')->name('web.danhmuc');
Route::get('chitiet/{id}','App\Http\Controllers\Web\HomeController@getChitiet')->name('web.chitietsp');
///Cart
Route::post('cart','App\Http\Controllers\Web\HomeController@save')->name('web.cart');
Route::get('show-cart','App\Http\Controllers\Web\HomeController@show_cart')->name('cart');
Route::get('delete-cart/{rowId}','App\Http\Controllers\Web\HomeController@delete_cart')->name('deletecart');
Route::post('update-cart',[HomeController::class, 'update'])->name('update.cart');
Route::get('addToCart/{id}','App\Http\Controllers\Web\HomeController@addToCart')->name('addToCart');
///Check_out
Route::get('login_checkout','App\Http\Controllers\Web\HomeController@login_checkout')->name('login_checkout');
Route::get('register_checkout','App\Http\Controllers\Web\HomeController@register_checkout')->name('register_checkout');
Route::get('logout_checkout','App\Http\Controllers\Web\HomeController@logout_checkout')->name('logout_checkout');
Route::post('add_customer','App\Http\Controllers\Web\HomeController@add_customer')->name('add.customer');
Route::post('login','App\Http\Controllers\Web\HomeController@getLogin')->name('login');
Route::get('checkout','App\Http\Controllers\Web\HomeController@checkout')->name('checkout');
Route::post('save_checkout','App\Http\Controllers\Web\HomeController@save_checkout')->name('checkout.customer');
Route::post('order','App\Http\Controllers\Web\HomeController@order')->name('order');
Route::get('payment','App\Http\Controllers\Web\HomeController@payment')->name('payment');
Route::get('don-hang','App\Http\Controllers\Web\HomeController@don_hang')->name('don_hang');

Route::get('lien-he','App\Http\Controllers\Web\HomeController@lienHe')->name('lien_he');
Route::get('huong-dan-dat-hang','App\Http\Controllers\Web\HomeController@orderingGuide')->name('ordering_guide');
Route::get('phuong-thuc-giao-hang','App\Http\Controllers\Web\HomeController@giaohang')->name('giaohang');
Route::get('phuong-thuc-thanh-toan','App\Http\Controllers\Web\HomeController@thanhtoan')->name('thanhtoan');
Route::get('gioi-thieu','App\Http\Controllers\Web\HomeController@gioithieu')->name('gioithieu');
Route::get('tin-tuc','App\Http\Controllers\Web\HomeController@tintuc')->name('tintuc');
Route::get('uu-dai','App\Http\Controllers\Web\HomeController@uu_dai')->name('uu_dai');
