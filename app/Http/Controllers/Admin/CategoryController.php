<?php

namespace App\Http\Controllers\Admin;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
        return Category::all();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $category = new Category();
        $category->ca_name = $request->ca_name;
        $category->parent_id=$request->parent_id;
        $category->save();
        if ($request->admin_view) {
            return redirect()->route('admin.category.index');
        }
        return $category;
    }
    public function show($id)
    {
        return Category::findOrFail($id);
    }
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->ca_name =  $request->ca_name;
        $category->parent_id=$request->parent_id;
        $category->save();
        if ($request->admin_view) {
            return redirect()->route('admin.category.index');
        }
        return $category;
    }

    public function destroy($id)
    {
        Category::where('ca_id',$id)->delete();
        return redirect()->route('admin.category.index');
    }

}

