<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return User::all();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $User = new User();
        $User->name_user = $request->name_user;
        $User->email =  $request->email;
        $User->password = $request->password;
        $User->phone =  $request->phone;
        $User->address = $request->address;
        $User->gender =  $request->gender;
        $User->role = $request->role;
        $User->save();
        if ($request->admin_view) {
            return redirect()->route('admin.user.index');
        }
        return $User;
    }
    public function show($id)
    {
        return User::findOrFail($id);
    }
    public function update(Request $request, $id)
    {
        $User = User::find($id);
        $User->name_user = $request->name_user;
        $User->email =  $request->email;
        // $User->password = $request->password;
        $User->phone =  $request->phone;
        $User->address = $request->address;
        $User->gender =  $request->gender;
        $User->role = $request->role;
        $User->save();
        if ($request->admin_view) {
            return redirect()->route('admin.user.index');
        }
        return $User;
    }

    public function destroy($id)
    {
        User::where('id_user',$id)->delete();
        return redirect()->route('admin.user.index');
    }
}

