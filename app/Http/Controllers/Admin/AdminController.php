<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\sanpham;
use App\Models\User;
use App\Models\ncc;
use App\Models\Order;
use Carbon\Carbon;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function index()
    {
        $count_sanpham = DB::table('sanpham')->sum('soluong');
        $count_ton_kho = $count_sanpham;
        $tong_don = DB::table('order')->count('order_id');
        $tong_tien = DB::table('order')->sum('tongtien');
        $tong_khach_hang = DB::table('customer')->count('ct_id');
        return view('admin.home', compact('tong_tien', 'tong_don', 'tong_khach_hang'));
    }

    ////Category///
    public function getCategory()
    {
        $category = Category::all();
        foreach ($category as $item) {

        }
        return view('admin.theloai.danhsach', compact('category'));
    }

    public function createCategory()
    {
        $category= Category::where('parent_id',0)->get();
        return view('admin.theloai.them',compact('category'));
    }

    public function updateCategory($id)
    {
        $category = DB::table('category')->where('ca_id', $id)->first();
        return view('admin.theloai.sua', compact('category'));
    }


public function getNCC()
    {
        $NCC = NCC::all();
        foreach ($NCC as $item) {
        }
        return view('admin.ncc.danhsach', compact('NCC'));
    }

    public function createNCC() {
        return view('admin.ncc.them');
    }

    public function updateNCC($id){
        $NCC=NCC::find($id);
        return view('admin.ncc.sua',compact('NCC'));
    }

    ///User
    public function getUser()
    {
        $User = User::all();
        foreach ($User as $item) {
        }
        return view('admin.user.danhsach', compact('User'));
    }

    public function createUser() {
        return view('admin.user.them');
    }

    public function updateUser($id){
        $User=User::find($id);
        return view('admin.user.sua',compact('User'));
    }
    ///sanpham//
    public function getSanPham()
    {
        $sanpham = sanpham::all();
        foreach ($sanpham as $item) {
        }

        return view('admin.sanpham.danhsach', compact('sanpham'));
    }

    public function createSanPham() {
        $category=Category::all();
        $NCC=ncc::all();
        foreach ($category as $item) {
        }
        foreach ($NCC as $item) {
        }
        return view('admin.sanpham.them',compact('category','NCC'));
    }

    public function updateSanPham($id){
        $sanpham=sanpham::find($id);
        $category=Category::all();
        $NCC=ncc::all();
        foreach ($category as $item) {
        }
        foreach ($NCC as $item) {
        }
        return view('admin.sanpham.sua',compact('sanpham','category','NCC'));
    }

    public function order(){
        $all_order=DB::table('order')->join('customer','order.ct_id','=','customer.ct_id')->select('order.*','customer.ct_name')
            ->orderBy('order.order_id','desc')->get();

        return view('admin.cart.cart',compact('all_order'));
    }
    public function view_order($id){
        $orderbyId=DB::table('order')
            ->join('customer','order.ct_id','=','customer.ct_id')
            ->join('shipping','order.ship_id','=','shipping.ship_id')
            ->join('orderdetail','order.order_id','=','orderdetail.order_id')
            -> where('order.order_id', $id)
            ->select('order.*','shipping.*','orderdetail.*','customer.*')->first();

            $listOrder = DB::table('orderdetail')->where('order_id', $id)->get();
            // dd($listOrder);
        return view('admin.cart.view_order',compact('orderbyId', 'listOrder'));
    }

    public function thong_ke(){
        $count_sanpham = DB::table('sanpham')->sum('soluong');
//        $count_order = DB::table('orderdetail')->sum('soluong');
        $count_ton_kho = $count_sanpham;

//        $danh_thu_ngay = DB::table('')
//        dd($count_order);

//        $abc = DB::table('order')->select('created_at')->get();
//        dd($abc->day);

        // lay thang hien tai
        $currentMonth = Carbon::now()->month;
        // lay nam hien tai
        $currentYear = Carbon::now()->year;
        // lay ngay cuoi cung cua thang hien tai
        $lastCurrentMonth = date("Y-m-t", strtotime("0 month"));
        // lay nam thang hien tai
        $currentYM = $currentYear."-".$currentMonth;
        // tinh doanh thu va so luong don hang da ban theo thang
        $sum = DB::table('order')->whereBetween('created_at', [$currentYM.'-01 00:00:00', $lastCurrentMonth.' 23:59:59'])->sum('tongtien');
//        dd($sum);
        $count = DB::table('order')->whereBetween('created_at', [$currentYM.'-01 00:00:00', $lastCurrentMonth.' 23:59:59'])->count();

        // tinh doanh thu theo nam
        $sumY = DB::table('order')->whereBetween('created_at', [$currentYear.'-01-01 00:00:00', $currentYear.'-12-31 23:59:59'])->sum('tongtien');
        // luu doanh thu vao csdl
        $data = array();
        $dataY = array();
        // check doanh thu thang trong csdl
        $select = DB::table('revenues')->where('thang', $currentYM)->first();
        if($select) { // neu da co doanh thu thang nay thi update du lieu
            // thuc thi lenh update doanh thu vao csdl
            $data['tong_doanh_thu'] = $sum;
            $data['tong_don_hang'] = $count;
            DB::table('revenues')->where('thang', $currentYM)->update($data);
        } else { // neu chua co doanh thu thang nay thi them vao csdl
            // thuc thi lenh luu doanh thu vao csdl
            $data['thang'] = $currentYM;
            $data['tong_doanh_thu'] = $sum;
            $data['tong_don_hang'] = $count;
            DB::table('revenues')->insert($data);
        }
        // check doanh thu nam trong csdl
        $selectY = DB::table('revenue_years')->where('nam', $currentYear)->first();
        if($selectY) { // neu da co doanh thu nam nay thi update du lieu
            // thuc thi lenh update doanh thu vao csdl
            $dataY['tong_doanh_thu'] = $sumY;
            DB::table('revenue_years')->where('nam', $currentYear)->update($dataY);
        } else { // neu chua co doanh thu nam nay thi them vao csdl
            // thuc thi lenh luu doanh thu vao csdl
            $dataY['nam'] = $currentYear;
            $dataY['tong_doanh_thu'] = $sumY;
            DB::table('revenue_years')->insert($dataY);
        }
        // lich su doanh thu
        $history = DB::table('revenues')->select('*')->orderBy('thang', 'desc')->get();
        $historyY = DB::table('revenue_years')->select('*')->orderBy('nam', 'desc')->get();
        // $ban_chay = DB::table('orderdetail')->select(count('soluong'))->groupBy('b_id');
        $ban_chay = DB::select('select count(soluong) as tong_so_luong, b_id, b_name from orderdetail group by b_id, b_name');
        $so_luong_max = 0;
        foreach($ban_chay as $key){
            if($key->tong_so_luong > $so_luong_max){
                $so_luong_max = $key->tong_so_luong;
                $b_name = $key->b_name;
            }
            
        }
        // $ban_chay = Select * from tenbang whare soluong=( select count(soluong) from tenbang group by id
        return view('admin.layout.thong-ke', compact('count_ton_kho','sum', 'count', 'currentMonth', 'currentYear', 'history', 'historyY', 'so_luong_max', 'b_name'));
    }

    public function complete($id){
        // dd($id);
        Order::where('order_id',$id)->update(['trangthai' => 'Hoàn thành']);
        return back();
    }

    public function doanhThu(){
        $orderproduct = OrderDetail::select(DB::raw("SUM(soluong) as count"))
            ->WhereYear('created_at', date('Y'))
            ->groupBy(DB::raw("Month(created_at)"))
            ->pluck('count');

        $months = OrderDetail::select(DB::raw("Month(created_at) as month"))
            ->WhereYear('created_at', date('Y'))
            ->groupBy(DB::raw("Month(created_at)"))
            ->pluck('month');

        $data = [0,0,0,0,0,0,0,0,0,0,0,0];

        foreach ($months as $index => $month) {
            --$month;
            $data[$month] = $orderproduct[$index];
        }

        $orderprice = Order::select(DB::raw("SUM(tongtien) as count"))
            ->WhereYear('created_at', date('Y'))
            ->groupBy(DB::raw("Month(created_at)"))
            ->pluck('count');
            
        $pricemonths = Order::select(DB::raw("Month(created_at) as month"))
            ->WhereYear('created_at', date('Y'))
            ->groupBy(DB::raw("Month(created_at)"))
            ->pluck('month');

        $dataprice = [0,0,0,0,0,0,0,0,0,0,0,0];

        foreach ($pricemonths as $index => $pricemonth) {
            --$pricemonth;
            $dataprice[$pricemonth] = $orderprice[$index];
        }

        return view('admin.layout.doanh-thu', [
            'dataprice' => $dataprice,
            'data' => $data,
        ]);
    }
}
