<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class AuthController extends Controller
{
    public function login(){
        return view('admin.login');
    }

    public function loginPost(Request $request){
        // dd($request->all());
//        dd($request->all());
//        $credentials = $request->validate([
//            'email' => ['required', 'email'],
//            'password' => ['required'],
//        ]);
//        dd(Auth);
//        dd($credentials);

//        dd(Auth::attempt($request->only('email','password')));
        if (Auth::attempt($request->only('email','password'))){
            return redirect()->route('admin.home')->with('success','Đăng nhập thành công');
        }
        else{
            return redirect()->back()->with('error','Đăng nhập không thành công');
        }
    }

    public function logout(){
//        dd('abc');
        Auth::logout();
        return redirect()->route('admin.login');
    }
    public function register(){
        return view('admin.register');
    }

    public function registerPost(Request $request){
//        dd($request->all());
        $request->merge(['password'=>Hash::make($request->password)]);
//        dd($request->all());
        User::create($request->all());
        return redirect()->route('admin.login')->with('success','Đăng ký thành công');

    }
}
