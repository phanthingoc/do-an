<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ncc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NCCController extends Controller
{
    public function index()
    {
        return NCC::all();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $NCC = new NCC();
        $NCC->ncc_name = $request->ncc_name;
        $NCC->sdt =  $request->sdt;
        $NCC->email =  $request->email;
        $NCC->save();
        if ($request->admin_view) {
            return redirect()->route('admin.NCC.index');
        }
        return $NCC;
    }
    public function show($id)
    {
        return NCC::findOrFail($id);
    }
    public function update(Request $request, $id)
    {
        $NCC = NCC::find($id);
        $NCC->ncc_name =  $request->ncc_name;
        $NCC->sdt =  $request->sdt;
        $NCC->email =  $request->email;
        $NCC->save();
        if ($request->admin_view) {
            return redirect()->route('admin.NCC.index');
        }
        return $NCC;
    }

    public function destroy($id)
    {
        NCC::where('ncc_id',$id)->delete();
        return redirect()->route('admin.NCC.index');
    }
}

