<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AllRequest;
use App\Models\sanpham;
use Illuminate\Http\Request;

class SanphamController extends Controller
{
    public function index()
    {
        return sanpham::all();
        foreach ($sanpham as $item)
        {
            $item->category_id = $item->category->ca_name;
            $item->ncc_id = $item->ncc->ncc_name;
        }
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $sanpham = new sanpham();
        $sanpham->b_name = $request->b_name;
        if ($request->hasFile('b_image')) {
            $file = $request->file('b_image');
            $duoi = $file->getClientOriginalExtension();
            if ($duoi != 'jpg' && $duoi != 'png') {
                return redirect()->route('admin.sanpham.create')->with('error', 'Bạn chỉ được chọn file có đuôi png,jpg');
            }
            $name = $file->getClientOriginalName();
            $image = random_int(100, 999) . "_" . $name;
            while (file_exists('upload/sanpham/' . $image)) {
                $image = random_int(100, 999) . "_" . $name;
            }
            $file->move('upload/sanpham/', $image);

            $sanpham->b_image = $image;
        }
        $sanpham->mota=$request->mota;
        $sanpham->category_id = $request->category_id;
        $sanpham->ncc_id = $request->ncc_id;
        $sanpham->gia = $request->gia;
        $sanpham->soluong = $request->soluong;
        $sanpham->sale = $request->sale;
        $sanpham->save();
        if ($request->admin_view) {
            return redirect()->route('admin.sanpham.index');
        }
        return $sanpham;
    }
    public function show($id)
    {
        return sanpham::findOrFail($id);
        $sanpham->category_id = $sanpham->category->name;
        return $product;
    }
    public function update(Request $request, $id)
    {
        $sanpham = sanpham::find($id);
        $sanpham->b_name = $request->b_name;
        if ($request->hasFile('b_image')) {
            $file = $request->file('b_image');
            $duoi = $file->getClientOriginalExtension();
            if ($duoi != 'jpg' && $duoi != 'png') {
                return redirect()->route('admin.sanpham.create')->with('error', 'Bạn chỉ được chọn file có đuôi png,jpg');
            }
            $name = $file->getClientOriginalName();
            $image = random_int(100, 999) . "_" . $name;
            while (file_exists('upload/noithat/' . $image)) {
                $image = random_int(100, 999) . "_" . $name;
            }
            $file->move('upload/noithat/', $image);
            if (file_exists('upload/noithat/' . $sanpham->b_image)) {
                unlink("upload/noithat/" . $sanpham->b_image);
            }
            $sanpham->b_image = $image;
        }
        $sanpham->mota=$request->mota;
        $sanpham->category_id = $request->category_id;
        $sanpham->ncc_id = $request->ncc_id;
        $sanpham->gia = $request->gia;
        $sanpham->soluong = $request->soluong;
        $sanpham->sale = $request->sale;
        $sanpham->save();
        if ($request->admin_view) {
            return redirect()->route('admin.sanpham.index');
        }
        return $sanpham;
    }

    public function destroy($id)
    {
        $sanpham = sanpham::find($id);
        if (file_exists('upload/noithat/' . $sanpham->b_image)) {
            unlink("upload/noithat/" . $sanpham->b_image);
        }
        $sanpham->delete();
        return redirect()->route('admin.sanpham.index');
    }

}

