<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\sanpham;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Gloudemans\Shoppingcart\Facades\Cart;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{

    public function index()
    {
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();
        $sanpham = DB::table('sanpham')->orderBy('b_id', 'desc')->limit(9)->get();
        $sanpham1 = DB::table('sanpham')->orderBy('b_id', 'desc')->paginate('8');
        $sanpham2 = DB::table('sanpham')->orderBy('b_id', 'desc')->paginate('5');
        return view('webb.home', compact('category', 'sanpham', 'sanpham1', 'sanpham2'));
    }
    public function getDanhmuc($id_c)
    {
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();
        $sanpham = DB::table('sanpham')->orderBy('b_id', 'desc')->get();
        $cate_by_id = DB::table('sanpham')->join('category', 'sanpham.category_id', '=', 'category.ca_id')->where('sanpham.category_id', $id_c)->get();
        $ca_name = DB::table('category')->where('category.ca_id', $id_c)->limit(1)->get();
        return view('webb.danhmuc.show_category', compact('cate_by_id', 'category', 'ca_name', 'sanpham'));
    }

    public function getChitiet($id)
    {
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();
        $detail_sanpham = DB::table('sanpham')->join('category', 'category.ca_id', '=', 'sanpham.category_id')
            ->join('ncc', 'ncc.ncc_id', '=', 'sanpham.ncc_id')
            ->where('sanpham.b_id', $id)->get();
        foreach ($detail_sanpham as $key => $value)
            $category_id = $value->category_id;
        $splienquan = DB::table('sanpham')->join('category', 'category.ca_id', '=', 'sanpham.category_id')
            ->join('ncc', 'ncc.ncc_id', '=', 'sanpham.ncc_id')
            ->where('sanpham.category_id', $category_id)->whereNotIn('sanpham.b_id', [$id])->get();
        return view('webb.sanpham.detail_sp', compact('category', 'detail_sanpham', 'splienquan', 'category_id'));
    }
    public function save(Request $request)
    {
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();
        $sanphamid = $request->productid_hidden;
        $quantity = $request->qty;
        $info = DB::table('sanpham')->where('b_id', $sanphamid)->first();
        $data['id'] = $info->b_id;
        $data['qty'] = $quantity;
        $data['name'] = $info->b_name;
        $data['price'] = $info->gia - $info->gia*($info->sale/100);
        $data['weight'] = $info->gia;
        $data['options']['image'] = $info->b_image;
        Cart::add($data);
        Cart::setGlobalTax(0);
        return Redirect::to('/show-cart');
    }

    public function show_cart()
    {
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();
        //        dd('a')
        return view('webb.cart', compact('category'));
    }
    public function delete_cart(Request $request, $id)
    {
        Cart::update($id, 0);
        return redirect()->route('cart');
    }
    public function update(Request $request)
    {
        //            dd($request->all());
        $id = $request->rowIdcart;
        $quantityc = $request->quantityc;
        Cart::update($id, $quantityc);
        return redirect()->route('cart');
    }
    public function login_checkout()
    {
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();
        return view('webb.login.login', compact('category'));
    }
    public function register_checkout()
    {
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();
        return view('webb.login.register', compact('category'));
    }
    public function add_customer(Request $request)
    {
        $data = array();
        $data['ct_name'] = $request->ct_name;
        $data['sdt'] = $request->sdt;
        $data['email'] = $request->email;
        $data['password'] = md5($request->password);
        $ct_id = DB::table('customer')->insertGetId($data);

        Session::put('ct_id', $ct_id);
        Session::put('ct_name', $request->ct_name);

        return Redirect::to('/checkout');
    }

    public function checkout()
    {
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();
        $carts = Cart::content();
        foreach($carts as $cart){
            $sanpham = DB::table('sanpham')->where('b_id', $cart->id)->first();
            if($cart->qty > $sanpham->soluong){
                alert()->error('Số lượng đặt hàng vượt quá số lượng trong kho');
                return back();
            }
        }
        return view('webb.login.checkout', compact('category'));
    }
    public function don_hang() {
        $list =  DB::table('orderdetail')
            ->join('order', 'order.order_id', '=', 'orderdetail.order_id')
            ->select(
                'orderdetail.*',
            )->where('order.ct_id', Session::get('ct_id'))->get();
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();
        return view('webb.don_hang', compact('category', 'list'));
    }

    public function save_checkout(Request $request)
    {
        $data = array();
        $data['ship_name'] = $request->ship_name;
        $data['ship_address'] = $request->ship_address;
        $data['ship_phone'] = $request->ship_phone;
        $data['ship_email'] = $request->ship_email;
        $ship_id = DB::table('shipping')->insertGetId($data);

        Session::put('ship_id', $ship_id);

        return Redirect::to('/payment');
    }

    public function payment()
    {
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();
        return view('webb.login.checkout', compact('category'));
    }
    public function execPostRequest($url, $data)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            )
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        //execute post
        $result = curl_exec($ch);
        //close connection
        curl_close($ch);
        return $result;
    }

    public function order(Request $request)
    {
        //lấy hình thức thanh toán
        $data = array();
        $data['payment_method'] = $request->check_method;
        $data['payment_status'] = 'Đang chờ xử lý';
        $payment_id = DB::table('payment')->insertGetId($data);
        //thêm đăt hàng
        $o_data = array();
        $o_data['ct_id'] = Session::get('ct_id');
        $o_data['ship_id'] = Session::get('ship_id');
        $o_data['payment_id'] = $payment_id;
        $o_data['tongtien'] = Cart::total();
        $o_data['tongtien'] = str_replace(',', '', $o_data['tongtien']);
        if ($data['payment_method'] == 1) {
            $o_data['trangthai'] = 'Đang chờ xử lý';
        }else if ($data['payment_method'] == 3) {
            $o_data['trangthai'] = 'Đã thanh toán';
        }
        $order_id = Order::create($o_data)->id;
        //        dd($order_id);
        //        $order_id=DB::table('order')->insertGetId($o_data);
        //        $order_id=DB::table('order')->insertGetId($o_data);
        ///chi tiết
        $content = Cart::content();
        foreach ($content as $v_content) {
            $d_data['order_id'] = $order_id;
            $d_data['b_id'] = $v_content->id;
            $d_data['b_name'] = $v_content->name;
            $d_data['gia'] = $v_content->price;
            $d_data['soluong'] = $v_content->qty;
            $d_data['created_at'] = Carbon::now();
            DB::table('orderdetail')->insert($d_data);

            $sanpham = sanpham::find($d_data['b_id']);
            $sanpham->soluong = $sanpham->soluong - $d_data['soluong'];
            $sanpham->save();
        }
        if ($data['payment_method'] == 1) {
            Cart::destroy();
            alert()->success('Đặt hàng thành công');
            return redirect()->route('home')->with('thongbao', 'Bạn đặt hàng thành công!');
        }
        else if ($data['payment_method'] == 3) {
            
            // $config = file_get_contents('../config.json');
            // $array = json_decode($config, true);

            // include "../common/helper.php";
            $o_data['tongtien'] = str_replace('.00', '', $o_data['tongtien']);

            $endpoint = "https://test-payment.momo.vn/v2/gateway/api/create";


            $partnerCode = 'MOMOBKUN20180529';
            $accessKey = 'klm05TvNBzhg7h7j';
            $secretKey = 'at67qH6mk8w5Y1nAyMoYKMWACiEi2bsa';
            $orderInfo = "Thanh toán qua MoMo";
            $amount = $o_data['tongtien'];
            // dd($o_data['tongtien']);
            $orderId = time() . "";
            $redirectUrl = "http://127.0.0.1:8000";
            $ipnUrl = "http://127.0.0.1:8000";
            $extraData = "";



                $requestId = time() . "";
                $requestType = "payWithATM";
                // $extraData = ($_POST["extraData"] ? $_POST["extraData"] : "");
                //before sign HMAC SHA256 signature
                $rawHash = "accessKey=" . $accessKey . "&amount=" . $amount . "&extraData=" . $extraData . "&ipnUrl=" . $ipnUrl . "&orderId=" . $orderId . "&orderInfo=" . $orderInfo . "&partnerCode=" . $partnerCode . "&redirectUrl=" . $redirectUrl . "&requestId=" . $requestId . "&requestType=" . $requestType;
                $signature = hash_hmac("sha256", $rawHash, $secretKey);
                // dd($signature);
                $data = array('partnerCode' => $partnerCode,
                    'partnerName' => "Test",
                    "storeId" => "MomoTestStore",
                    'requestId' => $requestId,
                    'amount' => $amount,
                    'orderId' => $orderId,
                    'orderInfo' => $orderInfo,
                    'redirectUrl' => $redirectUrl,
                    'ipnUrl' => $ipnUrl,
                    'lang' => 'vi',
                    'extraData' => $extraData,
                    'requestType' => $requestType,
                    'signature' => $signature);
                $result = $this->execPostRequest($endpoint, json_encode($data));
                // dd($result);
                $jsonResult = json_decode($result, true);  // decode json
                Cart::destroy();
                alert()->success('Đặt hàng thành công');
                //Just a example, please check more in there
                return redirect()->to($jsonResult['payUrl']);
                
                // header('Location: ' . $jsonResult['payUrl']);
            // }
            // alert()->success('Đặt hàng thành công');
            // return redirect()->route('home')->with('thongbao', 'Bạn đặt hàng thành công!');
            }
            
        return Redirect::to('/payment');
    }
    public function logout_checkout()
    {
        Session::flush();
        return Redirect::to('/login_checkout');
    }

    public function getLogin(Request $request)
    {
        $email = $request->email_account;
        $password = md5($request->password_account);
        $result = DB::table('customer')->where('email', $email)->where('password', $password)->first();
        if ($result) {
            Session::put('ct_id', $result->ct_id);
            return Redirect::to('/checkout');
        } else {
            return Redirect::to('/login_checkout');
        }
    }
    public function search(Request $request)
    {
        $keywords = $request->keyword_submit;
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();
        $search = DB::table('sanpham')->where('b_name', 'like', '%' . $keywords . '%')->get();
        return view('webb.sanpham.search', compact('category', 'search'));
    }

    public function addToCart($id)
    {
        // dd($id);
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();
        $sanphamid = $id;
        $quantity = 1;
        $info = DB::table('sanpham')->where('b_id', $sanphamid)->first();
        $data['id'] = $info->b_id;
        $data['qty'] = $quantity;
        $data['name'] = $info->b_name;
        $data['price'] = $info->gia - $info->gia*($info->sale/100);
        $data['weight'] = $info->gia;
        $data['options']['image'] = $info->b_image;
        Cart::add($data);
        Cart::setGlobalTax(0);
        return back();
    }

    public function lienHe()
    {
        // dd('a')
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();

        return view('webb.lien-he', compact('category'));
    }

    public function orderingGuide()
    {
        // dd('a')
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();

        return view('webb.huong-dan-dat-hang', compact('category'));
    }

    public function thanhtoan()
    {
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();

        return view('webb.phuong-thuc-thanh-toan', compact('category'));
    }

    public function giaohang()
    {
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();

        return view('webb.phuong-thuc-giao-hang', compact('category'));
    }

    public function gioithieu()
    {
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();

        return view('webb.gioi-thieu', compact('category'));
    }

    public function tintuc()
    {
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();

        return view('webb.tin-tuc', compact('category'));
    }

    public function uu_dai()
    {
        $category = DB::table('category')->orderBy('ca_id', 'desc')->get();

        return view('webb.uu-dai', compact('category'));
    }
}
