<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class sanpham extends Model
{
    use SoftDeletes;
    protected $table='sanpham';
    protected $primaryKey = 'b_id';
    protected $fillable=[
        'b_name',
        'mota',
        'b_image',
        'category_id',
        'ncc_id',
        'gia',
        'soluong',
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\Category','category_id','ca_id');
    }
    public function ncc()
    {
        return $this->belongsTo('App\Models\ncc','ncc_id','ncc_id');
    }


}
