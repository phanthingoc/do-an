<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ncc extends Model
{
    use SoftDeletes;
    protected $table='ncc';
    protected $primaryKey = 'ncc_id';
    protected $fillable=[
        'ncc_name',
        'sdt',
        'email',
    ];

    public function sanpham()
    {
        $this->hasMany('App\Models\ncc','ncc_id','ncc_id');
    }
}
