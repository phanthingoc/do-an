<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected  $table='category';
    protected $primaryKey = 'ca_id';
    protected $fillable=['ca_name','parent_id'];

    public function sanpham()
    {
        return $this->hasMany('App\Models\sanpham','category_id','ca_id');
    }


}
