
@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
    <!--new product area start-->
<div class="new_product_area">
    <div class="block_title">
        <h3>Sản phẩm mới</h3>
    </div>

    <div class="row">
        <div class="product_active owl-carousel">
            @foreach($sanpham as $key => $cate)
            <div class="col-lg-3">
                <div class="single_product">
                    <div class="product_thumb">
                        <a href="{{route('web.chitietsp',$cate->b_id)}}"><img src="{{asset('upload/sanpham/'.$cate->b_image)}}" alt=""></a>
                        <div class="img_icone">
                            <img src="{{asset('upload/sanpham/'.$cate->b_image)}}" alt="">
                            @if($cate->sale != 0)
                            <div class="img_icone" style="background-color: red; color:white">
                                -{{ $cate->sale }}%
                           </div>
                           @endif
                        </div>
                        <div class="product_action">
                            
                            <a href="{{ route('addToCart',$cate->b_id) }}" > <i class="fa fa-shopping-cart"></i> Thêm vào giỏ hàng</a>
                        </div>
                    </div>
                    <div class="product_content">
                        {{-- <span class="product_price">{{number_format( $cate->gia)}} đ</span> --}}
                        <div class="content_price mb-15">
                            <span>{{ number_format( $cate->gia - $cate->gia*($cate->sale / 100)) }} đ</span>
                            @if($cate->sale != 0)
                            <span class="old-price">{{number_format( $cate->gia)}} đ</span>
                            @endif
                        </div>
                        <h3 class="product_title"><a href="{{route('web.chitietsp',$cate->b_id)}}">{{$cate->b_name}}</a></h3>
                    </div>
                    <div class="product_info">
                        <ul>
                            <li><a href="{{route('web.chitietsp',$cate->b_id)}}" title="Quick view">Chi tiết</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<!--new product area start-->


<!--banner area start-->
<div class="banner_area mb-60">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="single_banner">
                <a href="#"><img src="{{asset('Web1\assets\img\banner\banner7.jpg')}}" alt=""></a>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="single_banner">
                <a href="#"><img src="{{asset('Web1\assets\img\banner\banner8.jpg')}}" alt=""></a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <!--shop toolbar start-->
            <div class="shop_toolbar list_toolbar mb-35">
                <div class="list_button">
                    <ul class="nav" role="tablist">
                        <li>
                            <a data-toggle="tab" href="#large" role="tab" aria-controls="large" aria-selected="true"><i class="fa fa-th-large"></i></a>
                        </li>
                        <li>
                            <a class="active" data-toggle="tab" href="#list" role="tab" aria-controls="list" aria-selected="false"><i class="fa fa-th-list"></i></a>
                        </li>
                    </ul>
                </div>
{{--                <div class="page_amount">--}}
{{--                    <p>Showing 1–9 of 21 results</p>--}}
{{--                </div>--}}
{{--                <div class="select_option">--}}
{{--                    <form action="#">--}}
{{--                        <label>Sort By</label>--}}
{{--                        <select name="orderby" id="short">--}}
{{--                            <option selected="" value="1">Position</option>--}}
{{--                            <option value="1">Price: Lowest</option>--}}
{{--                            <option value="1">Price: Highest</option>--}}
{{--                            <option value="1">Product Name:Z</option>--}}
{{--                            <option value="1">Sort by price:low</option>--}}
{{--                            <option value="1">Product Name: Z</option>--}}
{{--                            <option value="1">In stock</option>--}}
{{--                            <option value="1">Product Name: A</option>--}}
{{--                            <option value="1">In stock</option>--}}
{{--                        </select>--}}
{{--                    </form>--}}
{{--                </div>--}}
            </div>
            <!--shop toolbar end-->
        </div>
    </div>

    <!--shop tab product-->
    <div class="shop_tab_product shop_fullwidth">
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade " id="large" role="tabpanel">
                <div class="row">
                    @foreach($sanpham1 as $key => $cate)
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="single_product">
                            <div class="product_thumb">
                                <a href="{{route('web.chitietsp',$cate->b_id)}}"><img src="{{asset('upload/sanpham/'.$cate->b_image)}}" alt=""></a>
                                <div class="img_icone">
                                    <img src="{{asset('upload/sanpham/'.$cate->b_image)}}" alt="">
                                    @if($cate->sale != 0)
                                    <div class="img_icone" style="background-color: red; color:white">
                                        -{{ $cate->sale }}%
                                    </div>
                                    @endif
                                </div>
                                <div class="product_action">
                                    {{-- <form action="{{route('web.cart')}} " method="POST" hidden>
                                        {{csrf_field()}}
                                        <input name="qty" type="number" value="1" hidden />
                                        <input name="productid_hidden" type="hidden" value="{{$cate->b_id}}" />
                                        <a href=""><button type="submit"><i class="fa fa-shopping-cart"></i> Add to cart</button></a>

                                    </form> --}}
                                    <a href="{{ route('addToCart',$cate->b_id) }}"> <i class="fa fa-shopping-cart"></i> Add to cart</a>
                                </div>
                            </div>
                            <div class="product_content">
                                <div class="content_price mb-15">
                                    <span>{{ number_format( $cate->gia - $cate->gia*($cate->sale / 100)) }} đ</span>
                                    @if($cate->sale != 0)
                                    <span class="old-price">{{number_format( $cate->gia)}} đ</span>
                                    @endif
                                </div>
                                <h3 class="product_title"><a href="{{route('web.chitietsp',$cate->b_id)}}">{{$cate->b_name}}</a></h3>
                            </div>
                            <div class="product_info">
                                <ul>
                                    <li><a href="{{route('web.chitietsp',$cate->b_id)}}"  title="Quick view">Chi tiết</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="">
                    {{$sanpham1->appends(request()->all())->links()}}
                </div>
            </div>
            <div class="tab-pane fade show active" id="list" role="tabpanel">
                <div class="product_list_item mb-35">
                    @foreach($sanpham2 as $key => $cate)
                    <div class="row align-items-center">

                        <div class="col-lg-3 col-md-5 col-sm-6">
                            <div class="product_thumb">
                                <a href="{{route('web.chitietsp',$cate->b_id)}}"><img src="{{asset('upload/sanpham/'.$cate->b_image)}}" alt=""></a>
                                <div class="hot_img">
                                    <img src="{{asset('upload/sanpham/'.$cate->b_image)}}" alt="">
                                    @if($cate->sale != 0)
                            <div class="img_icone" style="background-color: red; color:white">
                                -{{ $cate->sale }}%
                           </div>
                           @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-7 col-sm-6">
                            <div class="list_product_content">
                                <div class="product_ratting">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="list_title">
                                    <h3><a href="{{route('web.chitietsp',$cate->b_id)}}"> {{$cate->b_name}}</a></h3>
                                </div>
                                <p class="design"> {{$cate->mota}}</p>

                                <div class="content_price mb-15">
                                    <span>{{ number_format( $cate->gia - $cate->gia*($cate->sale / 100)) }} đ</span>
                                    @if($cate->sale != 0)
                                    <span class="old-price">{{number_format( $cate->gia)}} đ</span>
                                    @endif
                                </div>
                                <div class="add_links">
                                    <ul>
                                        <li><a href="{{ route('addToCart',$cate->b_id) }}" title="add to cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
                                        <li><a href="{{route('web.chitietsp',$cate->b_id)}}"  title="Chi tiết"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="">
                    {{$sanpham2->appends(request()->all())->links()}}
                </div>
            </div>

        </div>
    </div>
    <!--shop tab product end-->

    <!--pagination style start-->
{{--    <div class="pagination_style shop_page">--}}
{{--        <div class="item_page">--}}
{{--            <form action="#">--}}
{{--                <label for="page_select">show</label>--}}
{{--                <select id="page_select">--}}
{{--                    <option value="1">9</option>--}}
{{--                    <option value="2">10</option>--}}
{{--                    <option value="3">11</option>--}}
{{--                </select>--}}
{{--                <span>Products by page</span>--}}
{{--            </form>--}}
{{--        </div>--}}
{{--        <div class="page_number">--}}
{{--            <span>Pages: </span>--}}
{{--            <ul>--}}
{{--                <li>«</li>--}}
{{--                <li class="current_number">1</li>--}}
{{--                <li><a href="#">2</a></li>--}}
{{--                <li>»</li>--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!--pagination style end-->
</div>

<!--banner area end-->

@endsection

@section('js')
    <script>
        $(document).ready(function(){
            console.log('aaaa');
            $('#saveBtn13').click(function (e) {
                // e.preventDefault();
                // $(this).html('Sending..');
                console.log('abc');
                $.ajax({
                    data: $('#CustomerForm').serialize(),
                    url: "",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {

                        $('#CustomerForm').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        table.draw();

                    },
                    error: function (data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Save Changes');
                    }
                });
            });
        });
    </script>
@endsection


