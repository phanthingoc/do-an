@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="/">home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Sản phẩm đã đặt</li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <div class="faq_content_area">
        <div class="row">
            <div class="col-12">
                <div class="table_desc">
                    <div class="cart_page table-responsive">
                        
                        <table>
                            <thead>
                            <tr>
                                <th class="product_name">Sản phẩm</th>
                                <th class="product-price">Giá</th>
                                <th class="product_quantity">Số lượng</th>
                                <th class="product_total">Thành tiền</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($list as $item)
                            <tr>
                            
                                <td class="product_name"><a href="#">{{ $item->b_name }}</a></td>
                                <td class="product-price">{{ $item->gia }} đ</td>
                                <td class="product_quantity">
                                    {{ $item->soluong }}
                                </td>
                                <td class="product_total">
                                    <?php
                                    $subtotal = $item->gia * $item->soluong;
                                    echo number_format($subtotal)
                                    ?>đ
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
