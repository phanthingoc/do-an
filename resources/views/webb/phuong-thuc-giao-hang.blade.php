
@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
    <div class="row">
        <div class="col-12">
            <div class="breadcrumb_content">
                <ul>
                    <li><a href="/">home</a></li>
                    <li><i class="fa fa-angle-right"></i></li>
                    <li>Phương thức giao hàng</li>
                </ul>

            </div>
        </div>
    </div>
</div>
<div class="faq_content_area">
    <div class="row">
        <div class="col-12">
            <div class="faq_content_wrapper">
                <h3>Công ty chúng tôi xin giới thiệu về chính sách vận chuyển như sau:</h3>
                <p><strong>1. Trong nội thành Hà Nội</strong></p>
                <p>Chúng tôi không căn cứ vào giá đơn hàng,số lượng sản phẩm ,khách hàng sẽ luôn được miễn phí vận chuyển khu vực nội thành với phạm vi nhỏ hơn 8 km.Với cự li lớn hơn 8km khách hàng sẽ chịu toàn bộ chi phí vận chuyển .</p>
                <p><strong>2. Ngoài nội thành Hà Nội</strong></p>
                <p>Phí vận chuyển được tính theo km và&nbsp;Hình thức vận chuyển ngoại thành :</p>
                <ul>
                    <li>Chúng tôi thuê xe,khách hàng chịu mọi chi phí vận chuyển .</li>
                    <li>Khách hàng quen nhà xe khách,vận tải,chúng tôi vận chuyển miễn phí giao cho nhà xe giúp khách hàng.Quý khách chịu toàn bộ chi phí vận chuyển do nhà xe yêu cầu.</li>
                </ul>
                <p>&nbsp;</p>
                <h3>Mọi chi tiết xin liên hệ:</h3>
                <blockquote><p>NGỌC ÁNH FURNITURE</p>
                    <p>Website : ngocanh.net – ngocanh.vn</p>
                    <p>Điện thoại : 0984 283 671 – 0989 236 671</p></blockquote>
            </div>
        </div>
    </div>  
</div>
@endsection
