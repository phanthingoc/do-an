@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="/">home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Tin tức</li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <div class="faq_content_area">
        <div class="row">
            <div class="col-12">
                <div class="faq_content_wrapper">
                    <div class="row">


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="item_blog_owl">
                                <article class="blog-item blog-item-grid row">
                                    <div class="wrap_blg">
                                        <div class="blog-item-thumbnail img1 col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-top-5"
                                            onclick="window.location.href='/ban-an-thong-minh-xu-huong-cho-phong-an-hien-dai';">
                                            <a href="/ban-an-thong-minh-xu-huong-cho-phong-an-hien-dai">

                                                <picture>
                                                    <source media="(max-width: 480px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/6.jpg?v=1652422335823">
                                                    <source media="(min-width: 481px) and (max-width: 767px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/6.jpg?v=1652422335823">
                                                    <source media="(min-width: 768px) and (max-width: 1023px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/6.jpg?v=1652422335823">
                                                    <source media="(min-width: 1024px) and (max-width: 1199px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/6.jpg?v=1652422335823">
                                                    <source media="(min-width: 1200px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/6.jpg?v=1652422335823">
                                                    <img src="//bizweb.dktcdn.net/100/139/088/articles/6.jpg?v=1652422335823"
                                                        style="max-width:100%;" class="img-responsive"
                                                        alt="Bàn Ăn Thông Minh - Xu hướng cho Phòng Ăn hiện đại 2022">
                                                </picture>

                                            </a>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 content_ar blog_large_item">
                                            <h3 class="blog-item-name"><a class="text2line"
                                                    href="/ban-an-thong-minh-xu-huong-cho-phong-an-hien-dai"
                                                    title="Bàn Ăn Thông Minh - Xu hướng cho Phòng Ăn hiện đại 2022">Bàn Ăn
                                                    Thông Minh - Xu hướng cho Phòng Ăn hiện đại 2022</a></h3>
                                            <p class="meta">
                                                <span>13 Tháng 05 2022</span> <small>|</small>
                                                <span>Bài viết xem nhiều</span>
                                            </p>
                                            <div class="summary_ sum text4line">
                                                Bàn Ăn Thông Minh là chiếc bàn ăn có nhiều công năng, điều chỉnh thay đổi
                                                kích thước theo chiều dài. Tùy chỉnh theo mục đích người dùng ...
                                            </div>
                                            <a href="/ban-an-thong-minh-xu-huong-cho-phong-an-hien-dai"
                                                class="readmore">Đọc tiếp <i class="fa fa-angle-double-right"
                                                    aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="item_blog_owl">
                                <article class="blog-item blog-item-grid row">
                                    <div class="wrap_blg">
                                        <div class="blog-item-thumbnail img1 col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-top-5"
                                            onclick="window.location.href='/feedback-cua-khach-hang-bo-sofa-da-that-nhap-khau-divano-781-o-quang-ninh';">
                                            <a
                                                href="/feedback-cua-khach-hang-bo-sofa-da-that-nhap-khau-divano-781-o-quang-ninh">

                                                <picture>
                                                    <source media="(max-width: 480px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/800-sofa-da-divano-781.jpg?v=1640086661687">
                                                    <source media="(min-width: 481px) and (max-width: 767px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/800-sofa-da-divano-781.jpg?v=1640086661687">
                                                    <source media="(min-width: 768px) and (max-width: 1023px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/800-sofa-da-divano-781.jpg?v=1640086661687">
                                                    <source media="(min-width: 1024px) and (max-width: 1199px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/800-sofa-da-divano-781.jpg?v=1640086661687">
                                                    <source media="(min-width: 1200px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/800-sofa-da-divano-781.jpg?v=1640086661687">
                                                    <img src="//bizweb.dktcdn.net/100/139/088/articles/800-sofa-da-divano-781.jpg?v=1640086661687"
                                                        style="max-width:100%;" class="img-responsive"
                                                        alt="FeedBack của Khách Hàng: Bộ Sofa Da Thật Nhập Khẩu Divano 781 ở Quảng Ninh">
                                                </picture>

                                            </a>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 content_ar blog_large_item">
                                            <h3 class="blog-item-name"><a class="text2line"
                                                    href="/feedback-cua-khach-hang-bo-sofa-da-that-nhap-khau-divano-781-o-quang-ninh"
                                                    title="FeedBack của Khách Hàng: Bộ Sofa Da Thật Nhập Khẩu Divano 781 ở Quảng Ninh">FeedBack
                                                    của Khách Hàng: Bộ Sofa Da Thật Nhập Khẩu Divano 781 ở Quảng Ninh</a>
                                            </h3>
                                            <p class="meta">
                                                <span>21 Tháng 12 2021</span> <small>|</small>
                                                <span>Bài viết xem nhiều</span>
                                            </p>
                                            <div class="summary_ sum text4line">
                                                Customer Feedback: Sofa là trái tim phòng khách, thổi hồn cho không gian căn
                                                nhà. Cảm ơn Chị Thảo ở Quảng Ninh đã lựa chọn sản phẩm củ...
                                            </div>
                                            <a href="/feedback-cua-khach-hang-bo-sofa-da-that-nhap-khau-divano-781-o-quang-ninh"
                                                class="readmore">Đọc tiếp <i class="fa fa-angle-double-right"
                                                    aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="item_blog_owl">
                                <article class="blog-item blog-item-grid row">
                                    <div class="wrap_blg">
                                        <div class="blog-item-thumbnail img1 col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-top-5"
                                            onclick="window.location.href='/bo-sofa-da-bo-nhap-khau-divano-l-509-dep-khong-ti-vet-tai-nha-khach-hang';">
                                            <a
                                                href="/bo-sofa-da-bo-nhap-khau-divano-l-509-dep-khong-ti-vet-tai-nha-khach-hang">

                                                <picture>
                                                    <source media="(max-width: 480px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/8a-bo-sofa-da-tha-t-italia-nha-p-kha-u-l-509.jpg?v=1637328969050">
                                                    <source media="(min-width: 481px) and (max-width: 767px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/8a-bo-sofa-da-tha-t-italia-nha-p-kha-u-l-509.jpg?v=1637328969050">
                                                    <source media="(min-width: 768px) and (max-width: 1023px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/8a-bo-sofa-da-tha-t-italia-nha-p-kha-u-l-509.jpg?v=1637328969050">
                                                    <source media="(min-width: 1024px) and (max-width: 1199px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/8a-bo-sofa-da-tha-t-italia-nha-p-kha-u-l-509.jpg?v=1637328969050">
                                                    <source media="(min-width: 1200px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/8a-bo-sofa-da-tha-t-italia-nha-p-kha-u-l-509.jpg?v=1637328969050">
                                                    <img src="//bizweb.dktcdn.net/100/139/088/articles/8a-bo-sofa-da-tha-t-italia-nha-p-kha-u-l-509.jpg?v=1637328969050"
                                                        style="max-width:100%;" class="img-responsive"
                                                        alt="Bộ Sofa Da Bò Nhập Khẩu Divano L-509 Đẹp Không Tì Vết tại nhà Chị Thu - Quận Hà Đông">
                                                </picture>

                                            </a>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 content_ar blog_large_item">
                                            <h3 class="blog-item-name"><a class="text2line"
                                                    href="/bo-sofa-da-bo-nhap-khau-divano-l-509-dep-khong-ti-vet-tai-nha-khach-hang"
                                                    title="Bộ Sofa Da Bò Nhập Khẩu Divano L-509 Đẹp Không Tì Vết tại nhà Chị Thu - Quận Hà Đông">Bộ
                                                    Sofa Da Bò Nhập Khẩu Divano L-509 Đẹp Không Tì Vết tại nhà Chị Thu -
                                                    Quận Hà Đông</a></h3>
                                            <p class="meta">
                                                <span>19 Tháng 11 2021</span> <small>|</small>
                                                <span>Bài viết xem nhiều</span>
                                            </p>
                                            <div class="summary_ sum text4line">
                                                Chị Thu ở Quận Hà Đông tâm sự, mặc dù không có ý định mua sắm Sofa cho Gia
                                                Đình để tiết kiệm chi phí trong mùa dịch. Tuy nhiên, vô tình ...
                                            </div>
                                            <a href="/bo-sofa-da-bo-nhap-khau-divano-l-509-dep-khong-ti-vet-tai-nha-khach-hang"
                                                class="readmore">Đọc tiếp <i class="fa fa-angle-double-right"
                                                    aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="item_blog_owl">
                                <article class="blog-item blog-item-grid row">
                                    <div class="wrap_blg">
                                        <div class="blog-item-thumbnail img1 col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-top-5"
                                            onclick="window.location.href='/phong-khach-sang-trong-cua-nha-chi-hoai-biet-thu-an-khanh';">
                                            <a href="/phong-khach-sang-trong-cua-nha-chi-hoai-biet-thu-an-khanh">

                                                <picture>
                                                    <source media="(max-width: 480px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/8e-bo-sofa-da-tha-t-nha-p-kha-u-divano-s809.jpg?v=1635867926007">
                                                    <source media="(min-width: 481px) and (max-width: 767px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/8e-bo-sofa-da-tha-t-nha-p-kha-u-divano-s809.jpg?v=1635867926007">
                                                    <source media="(min-width: 768px) and (max-width: 1023px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/8e-bo-sofa-da-tha-t-nha-p-kha-u-divano-s809.jpg?v=1635867926007">
                                                    <source media="(min-width: 1024px) and (max-width: 1199px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/8e-bo-sofa-da-tha-t-nha-p-kha-u-divano-s809.jpg?v=1635867926007">
                                                    <source media="(min-width: 1200px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/8e-bo-sofa-da-tha-t-nha-p-kha-u-divano-s809.jpg?v=1635867926007">
                                                    <img src="//bizweb.dktcdn.net/100/139/088/articles/8e-bo-sofa-da-tha-t-nha-p-kha-u-divano-s809.jpg?v=1635867926007"
                                                        style="max-width:100%;" class="img-responsive"
                                                        alt="Phòng Khách Sang Trọng của nhà chị Hoài - Biệt Thự An Khánh">
                                                </picture>

                                            </a>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 content_ar blog_large_item">
                                            <h3 class="blog-item-name"><a class="text2line"
                                                    href="/phong-khach-sang-trong-cua-nha-chi-hoai-biet-thu-an-khanh"
                                                    title="Phòng Khách Sang Trọng của nhà chị Hoài - Biệt Thự An Khánh">Phòng
                                                    Khách Sang Trọng của nhà chị Hoài - Biệt Thự An Khánh</a></h3>
                                            <p class="meta">
                                                <span>02 Tháng 11 2021</span> <small>|</small>
                                                <span>Bài viết xem nhiều</span>
                                            </p>
                                            <div class="summary_ sum text4line">

                                                Sofa da thật Italia nhập khẩu chính hãng Divano S-809 sử dụng chất liệu da
                                                bò thương hiệu GM đến từ Italia với chất lượng &amp; độ bền cự...
                                            </div>
                                            <a href="/phong-khach-sang-trong-cua-nha-chi-hoai-biet-thu-an-khanh"
                                                class="readmore">Đọc tiếp <i class="fa fa-angle-double-right"
                                                    aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="item_blog_owl">
                                <article class="blog-item blog-item-grid row">
                                    <div class="wrap_blg">
                                        <div class="blog-item-thumbnail img1 col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-top-5"
                                            onclick="window.location.href='/feedback-cua-khach-hang-sofa-da-that-l844';">
                                            <a href="/feedback-cua-khach-hang-sofa-da-that-l844">

                                                <picture>
                                                    <source media="(max-width: 480px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/2-bo-sofa-da-tha-t-italia-divano-l844.jpg?v=1635521411600">
                                                    <source media="(min-width: 481px) and (max-width: 767px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/2-bo-sofa-da-tha-t-italia-divano-l844.jpg?v=1635521411600">
                                                    <source media="(min-width: 768px) and (max-width: 1023px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/2-bo-sofa-da-tha-t-italia-divano-l844.jpg?v=1635521411600">
                                                    <source media="(min-width: 1024px) and (max-width: 1199px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/2-bo-sofa-da-tha-t-italia-divano-l844.jpg?v=1635521411600">
                                                    <source media="(min-width: 1200px)"
                                                        srcset="//bizweb.dktcdn.net/thumb/large/100/139/088/articles/2-bo-sofa-da-tha-t-italia-divano-l844.jpg?v=1635521411600">
                                                    <img src="//bizweb.dktcdn.net/100/139/088/articles/2-bo-sofa-da-tha-t-italia-divano-l844.jpg?v=1635521411600"
                                                        style="max-width:100%;" class="img-responsive"
                                                        alt="Feedback của khách hàng: Sofa da thật L844">
                                                </picture>

                                            </a>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 content_ar blog_large_item">
                                            <h3 class="blog-item-name"><a class="text2line"
                                                    href="/feedback-cua-khach-hang-sofa-da-that-l844"
                                                    title="Feedback của khách hàng: Sofa da thật L844">Feedback của khách
                                                    hàng: Sofa da thật L844</a></h3>
                                            <p class="meta">
                                                <span>02 Tháng 11 2021</span> <small>|</small>
                                                <span>Bài viết xem nhiều</span>
                                            </p>
                                            <div class="summary_ sum text4line">
                                                Feedback của khách hàng. Anh Tuấn ở Cầu Giấy mất rất nhiều thời gian để tìm
                                                Sofa da thật cho Gia Đình. Cuối cùng, anh rất hài lòng khi c...
                                            </div>
                                            <a href="/feedback-cua-khach-hang-sofa-da-that-l844" class="readmore">Đọc
                                                tiếp <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
