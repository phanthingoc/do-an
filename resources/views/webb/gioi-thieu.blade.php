@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="/">home</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Giới thiệu</li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <div class="faq_content_area">
        <div class="row">
            <div class="col-12">
                <div class="faq_content_wrapper">
                    <div class="design over">
                        <h1>Giới thiệu Nội Thất Nhà Đẹp</h1>
                        <div class="row text over mg-bt content-ab">
                            <div class="col-xs-12 col-sm-6 mg-bt">
                                <h2 style="text-align: justify;"><span style="font-size:16px;"><span
                                            style="font-family:Roboto;">Công ty TNHH NGỌC ÁNH FURNITURE là
                                            công ty sở hữu thương hiệu “Nhà Đẹp”, hoạt động trong lĩnh vực sản xuất và kinh
                                            doanh đồ gỗ nội thất, đồ trang trí, gia dụng trong gia đình từ năm 1999. Nhà Đẹp
                                            là thành viên của TCT Group, một tập đoàn hoạt động trong các lĩnh vực như bán
                                            lẻ, kinh doanh bất động sản, sản xuất, xuất khẩu, kinh doanh nhà hàng, khách sạn
                                            và xây dựng nội thất. Cho tới nay TCT Group đã bao gồm&nbsp; gần 20 công ty
                                            thành viên với hàng loạt các chuỗi hệ thống quen thuộc như nhà hàng (PHỐ BIỂN,
                                            VẠN HOA, ĐỆ NHẤT, MAMMA MIA, HOLA, MARUTA), trung tâm thể thao (Fit24 Fitness
                                            &amp; Yoga), &nbsp;siêu thị nội thất (NHÀ ĐẸP)…</span></span></h2>

                                <p>Với phương châm: “Mang đến sự hài lòng cho tất cả các khách hàng”, Nhà Đẹp không chỉ đề
                                    cao chất lượng của từng sản phẩm mà còn đặc biệt chú trọng đến các dịch vụ đi kèm như tư
                                    vấn thiết kế, vận chuyển, lắp đặt, chăm sóc khách hàng, bảo hành, bảo trì …Chúng tôi
                                    luôn mong muốn khách hàng khi đến với Nhà Đẹp sẽ có được những lợi ích tốt nhất trước và
                                    sau khi sử dụng sản phẩm của công ty.</p>
                            </div>

                            <div class="col-xs-12 col-sm-6 mg-bt">
                                <p><img alt="Sofa da chụp tại showroom nội thất Nhà Đẹp"
                                        src="https://nhadep.com.vn/Uploads/images/trang-gioi-thieu/Sofa-da-noi-that-nha-dep.JPG"
                                        style="width: 100%; margin-top: 3px; margin-bottom: 3px;"></p>
                            </div>

                            <div class="col-xs-12 mg-bt">
                                <h3><span style="font-size:16px;"><span style="font-family:Roboto;">Hệ thống sản phẩm của
                                            Nhà Đẹp bao gồm đồ nội thất, đồ trang trí, gia dụng với mẫu mã, giá cả đa dạng,
                                            phù hợp với nhiều phong cách nội thất và kinh tế của từng gia đình. Đến với
                                            chúng tôi, chắc chắn bạn sẽ hoàn toàn hài lòng bởi những gì bạn cần cho tổ ấm
                                            của mình đều có tại Nhà Đẹp.</span></span></h3>

                                <p style="text-align: justify;">Sản phẩm&nbsp;<span style="text-align: justify;">của Nhà Đẹp
                                        đa dạng về mẫu mã, hợp lý về giá cả. Dù bạn đang sở hữu một ngôi nhà sang trọng,
                                        rộng rãi hay một căn hộ chung cư với diện tích vừa phải, bạn đều có thể lựa chọn cho
                                        mình những sản phẩm nội thất, gia dụng phù hợp và đồng bộ nhất cho ngôi nhà của mình
                                        tại Nhà Đẹp. Bất cứ phong cách nội thất nào bạn yêu thích, Nhà Đẹp sẽ luôn là điểm
                                        đến tin cậy cho sự lựa chọn của bạn.</span></p>
                            </div>

                        </div>
                        
                        <div class="slider-iw wmg slider slick-initialized slick-slider">
                            <div aria-live="polite" class="slick-list draggable">
                                <div class="slick-track" role="listbox"
                                    style="opacity: 1; width: 1140px; transform: translate3d(0px, 0px, 0px);">
                                    <div class="item pot over slick-slide slick-current slick-active" data-slick-index="0"
                                        aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide00"
                                        style="width: 285px;">
                                        
                                        <div class="oder over">
                                            
                                            <h3><a href="javascript:;" style="cursor:text;" title="Nội thất phòng khách"
                                                    class="blook" tabindex="0">Nội thất phòng khách</a></h3>
                                            <ul class="ul">
                                                <li><a class="blook" href="/sofa-da-phong-khach" tabindex="0"
                                                        title="Sofa vải">Sofa da</a></li>
                                                <li><a class="blook" href="/sofa-vai" tabindex="0"
                                                        title="Sofa vải">Sofa vải</a></li>
                                                <li><a class="blook" href="/sofa-giuong" tabindex="0"
                                                        title="Sofa giường">Sofa giường</a></li>
                                                <li><a class="blook" href="/ban-tra-hien-dai" tabindex="0"
                                                        title="Bàn trà">Bàn trà</a></li>
                                                <li><a class="blook" href="/tu-ke-phong-khach" tabindex="0"
                                                        title="Kệ tivi">Kệ tivi</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item pot over slick-slide slick-active" data-slick-index="1"
                                        aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide01"
                                        style="width: 285px;">
                                        
                                        <div class="oder over">
                                            
                                            <h3><a href="javascript:;" style="cursor:text;" title="Nội thất phòng ăn"
                                                    class="blook" tabindex="0">Nội thất phòng ăn</a></h3>
                                            <p>&nbsp;</p>

                                            <ul class="ul">
                                                <li><a class="blook" href="/ban-an-thong-minh-mo-rong" tabindex="0"
                                                        title="Bàn ăn thông minh">Bàn ăn thông minh</a></li>
                                                <li><a class="blook" href="/ghe-kim-loai" tabindex="0"
                                                        title="Ghế ăn bọc PU">Ghế ăn&nbsp;</a></li>
                                                <li><a class="blook" href="/do-dung-ban-an" tabindex="0"
                                                        title="Đồ gia dụng">Đồ dùng bàn ăn</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item pot over slick-slide slick-active" data-slick-index="2"
                                        aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide02"
                                        style="width: 285px;">
                                        
                                        <div class="oder over">
                                            
                                            <h3><a href="javascript:;" style="cursor:text;" title="Nội thất phòng ngủ"
                                                    class="blook" tabindex="0">Nội thất phòng ngủ</a></h3>
                                            <ul class="ul">
                                                <li><a class="blook" href="/giuong-ngu" tabindex="0"
                                                        title="Giường ngủ">Giường ngủ</a></li>
                                                <li><a class="blook" href="/tu-quan-ao" tabindex="0"
                                                        title="Tủ quần áo">Tủ quần áo</a></li>
                                                <li><a class="blook" href="/tu-dau-giuong" tabindex="0"
                                                        title="Tủ đầu giường">Tủ&nbsp;đầu giường</a></li>
                                                <li><a class="blook" href="/ban-trang-diem" tabindex="0"
                                                        title="Bàn trang điểm">Bàn trang&nbsp;điểm</a></li>
                                                <li><a class="blook" href="/ghe-thu-gian" tabindex="0"
                                                        title="Ghế thư giãn">Ghế thư giãn</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item pot over slick-slide slick-active" data-slick-index="3"
                                        aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide03"
                                        style="width: 285px;">
                                        
                                        <div class="oder over">
                                            
                                            <h3><a href="javascript:;" style="cursor:text;" title="Đồ trang trí, gia dụng"
                                                    class="blook" tabindex="0">Đồ trang trí, gia dụng</a></h3>
                                            <ul class="ul">
                                                <li><a class="blook" href="/tuong-trang-tri" tabindex="0">Tượng
                                                        trang trí</a></li>
                                                <li><a href="/tranh-khung-anh" tabindex="0">Tranh - Khung ảnh</a></li>
                                                <li><a class="blook" href="/tham-phong-khach" tabindex="0">Thảm
                                                        nhập khẩu</a></li>
                                                <li><a class="blook" href="/lo-hoa-trang-tri" tabindex="0">Lọ trang
                                                        trí</a></li>
                                                <li><a class="blook" href="/den-trang-tri" tabindex="0">Đèn trang
                                                        trí</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                        <div class="vews over">
                            <h2 class="pot text-center">MỘT SỐ HÌNH ẢNH THỰC TẾ TẠI SHOWROOM NỘI THẤT NHÀ ĐẸP</h2>
                            <div class="row" id="few-photo">
                                <div class="col-xs-12 col-sm-4 col-md-3"
                                    data-responsive="/Uploads/images/Tin-anh-su-kien/Anh-thuc-te-showroom/noi-that-nha-dep-1-1.jpg"
                                    data-src="/Uploads/images/Tin-anh-su-kien/Anh-thuc-te-showroom/noi-that-nha-dep-1-1.jpg">
                                    <div class="item pot">
                                        <a href="" title="nội thất nhà đẹp" class="blook delay">
                                            <div class="img wmg over">
                                                <img src="https://nhadep.com.vn/Uploads/images/Tin-anh-su-kien/Anh-thuc-te-showroom/noi-that-nha-dep-1-1.jpg"
                                                    class="img-responsive" alt="noi-that-nha-dep-1-1.jpg"
                                                    title="nội thất nhà đẹp">
                                            </div>
                                            
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-3"
                                    data-responsive="/Uploads/images/Tin-anh-su-kien/Anh-thuc-te-showroom/noi-that-nha-dep-10.JPG"
                                    data-src="/Uploads/images/Tin-anh-su-kien/Anh-thuc-te-showroom/noi-that-nha-dep-10.JPG">
                                    <div class="item pot">
                                        <a href="" title="nội thất nhà đẹp" class="blook delay">
                                            <div class="img wmg over">
                                                <img src="https://nhadep.com.vn/Uploads/images/Tin-anh-su-kien/Anh-thuc-te-showroom/noi-that-nha-dep-10.JPG"
                                                    class="img-responsive" alt="noi-that-nha-dep-10.JPG"
                                                    title="nội thất nhà đẹp">
                                            </div>
                                            
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-3"
                                    data-responsive="/Uploads/images/Tin-anh-su-kien/Anh-thuc-te-showroom/noi-that-nha-dep-11.JPG"
                                    data-src="https://nhadep.com.vn/Uploads/images/Tin-anh-su-kien/Anh-thuc-te-showroom/noi-that-nha-dep-11.JPG">
                                    <div class="item pot">
                                        <a href="" title="nội thất nhà đẹp" class="blook delay">
                                            <div class="img wmg over">
                                                <img src="https://nhadep.com.vn/Uploads/images/Tin-anh-su-kien/Anh-thuc-te-showroom/noi-that-nha-dep-11.JPG"
                                                    class="img-responsive" alt="noi-that-nha-dep-11.JPG"
                                                    title="nội thất nhà đẹp">
                                            </div>
                                            
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-3"
                                    data-responsive="https://nhadep.com.vn/Uploads/images/Tin-anh-su-kien/Anh-thuc-te-showroom/noi-that-nha-dep-12.JPG"
                                    data-src="https://nhadep.com.vn/Uploads/images/Tin-anh-su-kien/Anh-thuc-te-showroom/noi-that-nha-dep-12.JPG">
                                    <div class="item pot">
                                        <a href="" title="nội thất nhà đẹp" class="blook delay">
                                            <div class="img wmg over">
                                                <img src="https://nhadep.com.vn/Uploads/images/Tin-anh-su-kien/Anh-thuc-te-showroom/noi-that-nha-dep-12.JPG"
                                                    class="img-responsive" alt="noi-that-nha-dep-12.JPG"
                                                    title="nội thất nhà đẹp">
                                            </div>
                                            
                                        </a>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="vews-text text-justify over">
                                                <h3 class="text-center">Tầm nhìn</h3>
                                                <p><span style="font-size:16px;">Nội thất Nhà Đẹp&nbsp;xác định mục tiêu và
                                                        định hướng phát triển là trở thành một trong những công ty hàng đầu
                                                        trong lĩnh vực nội thất tại Việt Nam. Nhà Đẹp cung cấp các giải pháp
                                                        trọn gói từ tư vấn thiết kế, thi công lắp đặt và hoàn thiện mọi
                                                        thiết bị nội thất cho gia chủ.&nbsp;</span></p>

                                                <p>Nội thất Nhà Đẹp sẽ hiện thực hóa những ý tưởng, ước mơ của khách hàng và
                                                    mang đến cho khách hàng một không gian sống với phong cách nội thất hiện
                                                    đại, sang trọng, tiện nghi.</p>

                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="vews-text text-justify over">
                                                <h3 class="text-center">Sứ mệnh</h3>
                                                <p>Bằng chính tình yêu và sự trân trọng, Nội thất Nhà Đẹp luôn nổ lực không
                                                    ngừng để mang đến cho khách hàng những sản phẩm tốt nhất, kèm dịch vụ
                                                    tận tình, chu đáo nhất. Sự hài lòng của khách hàng là niềm tự hào của
                                                    chúng tôi.</p>

                                                <p>Với chúng tôi, kinh doanh đồ nội thất không chỉ là bán cho khách hàng một
                                                    sản phẩm mang tính vật chất, mà trên hết đó là giá trị tinh thần, chúng
                                                    tôi giúp bạn thổi hồn vào không gian sống, để từng phút giây bạn quây
                                                    quần bên gia đình là những phút giây thư giãn, bình yên và đắm
                                                    chìm&nbsp;trong hạnh phúc.</p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <script type="text/javascript">
                            var iw = 4;
                            if ($(window).width() < 990) {
                                iw = 3;
                            }
                            if ($(window).width() < 500) {
                                iw = 1;
                            }
                            $(document).on('ready', function() {
                                $(".slider-iw").slick({
                                    slidesToShow: iw,
                                    slidesToScroll: 1,
                                    autoplay: false,
                                    autoplaySpeed: 2000,
                                });
                            })
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
