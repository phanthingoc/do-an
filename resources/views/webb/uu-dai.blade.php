
@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
    <div class="row">
        <div class="col-12">
            <div class="breadcrumb_content">
                <ul>
                    <li><a href="/">home</a></li>
                    <li><i class="fa fa-angle-right"></i></li>
                    <li>Ưu đãi</li>
                </ul>

            </div>
        </div>
    </div>
</div>
<div class="faq_content_area">
    <div class="row">
        <div class="col-12">
            <div class="faq_content_wrapper">
                <div class="article-inner ">
                    <header class="entry-header">
                <div class="entry-header-text entry-header-text-top text-left">
                       <h6 class="entry-category is-xsmall">
                <a href="https://furnist.vn/tin-tuc/tin-khuyen-mai/" rel="category tag">Tin khuyến mãi</a>, <a href="https://furnist.vn/tin-tuc/" rel="category tag">Tin tức</a></h6>
            
            <h1 class="entry-title">Ưu đãi đặc biệt dành cho khách hàng thiết kế thi công nội thất trọn gói</h1>
            <div class="entry-divider is-divider small"></div>
            
                </div><!-- .entry-header -->
            
                    <div class="entry-image relative"> 
                  
                </div><!-- .entry-image -->
                </header><!-- post-header -->
                    <div class="entry-content single-page">
                <p><i><span style="font-weight: 400;">Bạn phải làm thế nào để sở hữu một không gian sống chất lượng cùng với những thiết kế hiện đại, tinh tế giữa MÙA DỊCH căng thẳng này ? Câu trả lời đó chính là ” Hãy đến với Furnist chúng tôi !”. Không chỉ sáng tạo, tận tâm trong từng thiết kế, mà chúng tôi còn mang đến những ƯU ĐÃI hấp dẫn đồng hành cùng mọi khách hàng vượt qua những khó khăn về kinh tế trong mùa dịch này. Chương trình “&nbsp;</span></i><b><i>ƯU ĐÃI</i></b><b><i>&nbsp;ĐẶC BIỆT MÙA DỊCH – DÀNH RIÊNG CHO KHÁCH HÀNG THIẾT KẾ THI CÔNG NỘI THẤT TRỌN GÓI</i></b><i><span style="font-weight: 400;">” có nội dung như sau :&nbsp;</span></i></p>
            <p><img style="display: block; margin-left: auto; margin-right: auto;" src="http://demo.furnist.vn/wp-content/uploads/2020/05/uu-dai-dac-biet-danh-cho-khach-hang-thiet-ke-thi-cong-noi-that-tron-goi-2953.jpg"></p>
            <ul>
            <li style="font-weight: 400;"><span style="font-weight: 400;">Tặng voucher giảm 30% khi mua sản phẩm nội thất + decor chất lượng của showroom Furnist.</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">Giảm giá tối đa 10% trên tổng giá trị hợp đồng thiết kế thi công trọn gói.</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">Quà tân gia HẤP DẪN: 1 Chậu hoa lan Hồ Điệp khi bàn giao nhà trị giá 2.000.000 Đ</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">Khách hàng được đội ngũ nhân viên tư vấn thiết kế theo các phong cách thiết kế thịnh hành đảm bảo mang lại không gian sống hoàn hảo nhất cùng các sản phẩm nội thất hiện đại, tiện nghi.</span></li>
            </ul>
            <p><b>Điều kiện áp dụng:&nbsp;</b></p>
            <p><span style="font-weight: 400;">Đơn hàng trên 100.000.000 Vnd</span></p>
            <p><b>Thời gian&nbsp; :&nbsp;</b></p>
            <p><span style="font-weight: 400;">Từ ngày 01/04/2020 đến 31/05/2020</span></p>
            <p><b><i>Lưu ý:</i></b></p>
            <ul>
            <li style="font-weight: 400;"><span style="font-weight: 400;">Voucher mua sắm được sử dụng khi còn nguyên vẹn, không rách vá, không áp dụng đồng thời với các chương trình khác.</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">Trong trường hợp có phát sinh vấn đề, quyết định cuối cùng thuộc về Furnist.</span></li>
            </ul>
            <p><span style="font-weight: 400;">NGỌC ÁNH FURNITURE luôn sẵn sàng giúp bạn hạnh phúc ở chính ngôi nhà bạn đang ở và trong từng khoảnh khắc bạn đang sống. Còn chần chờ gì mà không liên hệ ngay Furnist để sở hữu những thiết kế sáng tạo, mới lạ cùng những sản phẩm cực kì chất lượng với giá ưu đãi.</span></p>
            <p><span style="font-weight: 400;">NGỌC ÁNH FURNITURE – CÙNG CỘNG ĐỒNG CHUNG TAY ĐẨY LÙI COVID-19 !</span></p>
            <p><b><i>Thông tin liên hệ :</i></b></p>
            <p><span style="font-weight: 400;">Hotline:&nbsp;</span><b>19009417 – 0902.555.451 – 0384 203 548</b></p>
            <p><span style="font-weight: 400;">Showroom tại&nbsp;</span><b>Số 44 Đông Ngạc-Bắc Từ Liêm-Hà Nội</b><span style="font-weight: 400;">&nbsp;(Có chỗ để ô tô)</span></p>
            <p><span style="font-weight: 400;">Theo dõi fanpage&nbsp;</span><i><span style="font-weight: 400;">Facebook.com/Furnist.vn</span></i><span style="font-weight: 400;">&nbsp;để cập nhật các chương trình khuyến mãi.</span></p>
                            <div class="row">
                                
                                <div class="large-12 col">
                                
                                    <h3>Từ khóa</h3>
                                    <div class="entry-terms post-tags clearfix"> 
                                        <span class="terms-label"><i class="fa fa-tags"></i></span> 
                                        
            
                                    </div>
                                </div>
                            </div>
                            
                
                <div class="blog-share text-center"><div class="is-divider medium"></div><div class="social-icons share-icons share-row relative"><a href="whatsapp://send?text=%C6%AFu%20%C4%91%C3%A3i%20%C4%91%E1%BA%B7c%20bi%E1%BB%87t%20d%C3%A0nh%20cho%20kh%C3%A1ch%20h%C3%A0ng%20thi%E1%BA%BFt%20k%E1%BA%BF%20thi%20c%C3%B4ng%20n%E1%BB%99i%20th%E1%BA%A5t%20tr%E1%BB%8Dn%20g%C3%B3i - https://furnist.vn/uu-dai-dac-biet-danh-cho-khach-hang-thiet-ke-thi-cong-noi-that-tron-goi/" data-action="share/whatsapp/share" class="icon button circle is-outline tooltip whatsapp show-for-medium tooltipstered"><i class="icon-whatsapp"></i></a><a href="//www.facebook.com/sharer.php?u=https://furnist.vn/uu-dai-dac-biet-danh-cho-khach-hang-thiet-ke-thi-cong-noi-that-tron-goi/" data-label="Facebook" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="noopener noreferrer nofollow" target="_blank" class="icon button circle is-outline tooltip facebook tooltipstered"><i class="icon-facebook"></i></a><a href="//twitter.com/share?url=https://furnist.vn/uu-dai-dac-biet-danh-cho-khach-hang-thiet-ke-thi-cong-noi-that-tron-goi/" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="noopener noreferrer nofollow" target="_blank" class="icon button circle is-outline tooltip twitter tooltipstered"><i class="icon-twitter"></i></a><a href="mailto:enteryour@addresshere.com?subject=%C6%AFu%20%C4%91%C3%A3i%20%C4%91%E1%BA%B7c%20bi%E1%BB%87t%20d%C3%A0nh%20cho%20kh%C3%A1ch%20h%C3%A0ng%20thi%E1%BA%BFt%20k%E1%BA%BF%20thi%20c%C3%B4ng%20n%E1%BB%99i%20th%E1%BA%A5t%20tr%E1%BB%8Dn%20g%C3%B3i&amp;body=Check%20this%20out:%20https://furnist.vn/uu-dai-dac-biet-danh-cho-khach-hang-thiet-ke-thi-cong-noi-that-tron-goi/" rel="nofollow" class="icon button circle is-outline tooltip email tooltipstered"><i class="icon-envelop"></i></a><a href="//pinterest.com/pin/create/button/?url=https://furnist.vn/uu-dai-dac-biet-danh-cho-khach-hang-thiet-ke-thi-cong-noi-that-tron-goi/&amp;media=https://furnist.vn/wp-content/uploads/2020/05/uu-dai-dac-biet-thiet-ke-thi-cong-noi-that-tron-goi.png&amp;description=%C6%AFu%20%C4%91%C3%A3i%20%C4%91%E1%BA%B7c%20bi%E1%BB%87t%20d%C3%A0nh%20cho%20kh%C3%A1ch%20h%C3%A0ng%20thi%E1%BA%BFt%20k%E1%BA%BF%20thi%20c%C3%B4ng%20n%E1%BB%99i%20th%E1%BA%A5t%20tr%E1%BB%8Dn%20g%C3%B3i" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="noopener noreferrer nofollow" target="_blank" class="icon button circle is-outline tooltip pinterest tooltipstered"><i class="icon-pinterest"></i></a><a href="//www.linkedin.com/shareArticle?mini=true&amp;url=https://furnist.vn/uu-dai-dac-biet-danh-cho-khach-hang-thiet-ke-thi-cong-noi-that-tron-goi/&amp;title=%C6%AFu%20%C4%91%C3%A3i%20%C4%91%E1%BA%B7c%20bi%E1%BB%87t%20d%C3%A0nh%20cho%20kh%C3%A1ch%20h%C3%A0ng%20thi%E1%BA%BFt%20k%E1%BA%BF%20thi%20c%C3%B4ng%20n%E1%BB%99i%20th%E1%BA%A5t%20tr%E1%BB%8Dn%20g%C3%B3i" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="noopener noreferrer nofollow" target="_blank" class="icon button circle is-outline tooltip linkedin tooltipstered"><i class="icon-linkedin"></i></a></div></div></div><!-- .entry-content2 -->
            
            
            
                    <nav role="navigation" id="nav-below" class="navigation-post">
                <div class="flex-row next-prev-nav bt bb">
                    <div class="flex-col flex-grow nav-prev text-left">
                            <div class="nav-previous"><a href="https://furnist.vn/dong-hanh-vuot-dich-furnist-tro-gia-len-den-50/" rel="prev"><span class="hide-for-small"><i class="icon-angle-left"></i></span> ĐỒNG HÀNH VƯỢT DỊCH – FURNIST TRỢ GIÁ LÊN ĐẾN 50%</a></div>
                    </div>
                    <div class="flex-col flex-grow nav-next text-right">
                            <div class="nav-next"><a href="https://furnist.vn/top-phong-cach-thiet-ke-noi-that-duoc-ua-chuong-2020/" rel="next">Top phong cách thiết kế nội thất được ưa chuộng 2020 <span class="hide-for-small"><i class="icon-angle-right"></i></span></a></div>		</div>
                </div>
            
                    </nav><!-- #nav-below -->
            
                    </div>
            </div>
        </div>
    </div>  
</div>
@endsection
