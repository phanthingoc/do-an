
@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
    <div class="row">
        <div class="col-12">
            <div class="breadcrumb_content">
                <ul>
                    <li><a href="/">home</a></li>
                    <li><i class="fa fa-angle-right"></i></li>
                    <li>Phương thức thanh toán</li>
                </ul>

            </div>
        </div>
    </div>
</div>
<div class="faq_content_area">
    <div class="row">
        <div class="col-12">
            <div class="faq_content_wrapper">
                <h2><span id="Hai_hinh_thuc_thanh_toan_pho_bien_hien_nay"><strong>Hai hình thức thanh toán phổ biến hiện nay :</strong></span></h2>
                <ol>
                    <li>Thanh toán tiền mặt:
                    <ul>
                    <li>Quý khách có thể thanh toán cho người giao hàng hoặc nhân viên bán hàng của Nội thất Hòa Phát.</li>
                    <li>Nếu địa điểm giao hàng là Nội thành Hà nội đồng thời là địa điểm thanh toán và trị giá đơn hàng dưới 20 triệu (đã bao gồm thuế) thì chúng tôi sẽ thu tiền ngay khi giao hàng mà không cần đặt cọc.</li>
                    </ul>
                    </li>
                    <li>Thanh toán chuyển khoản qua ngân hàng, internet banking</li>
                </ol>
                <h3><span id="Quy_khach_hang_co_the_lua_chon_mot_trong_cac_tai_khoan_sau_de_nop_tien_chuyen_khoan">Quý khách hàng có thể lựa chọn một trong các tài khoản sau để nộp tiền, chuyển khoản:</span></h3>
                <p>1.<span style="font-family: 'times new roman', times; font-size: medium;"><strong>Tài Khoản MB bank :</strong></span></p>
                <ul>
                    <li><span style="font-family: 'times new roman', times; font-size: medium;"><span data-mce-mark="1">Số tài khoản :<strong> 5938666866666</strong></span></span></li>
                    <li><span style="font-family: 'times new roman', times; font-size: medium;"><span data-mce-mark="1">&nbsp;</span><span data-mce-mark="1">Chủ tài khoản : Phạm Thị Ngọc .<img class="lazy-load-active" src="file:///C:/Users/Admin/Desktop/z2563743987085_d64aadbdba6bbf9263e64c1f8e3dd011.jpg" data-src="file:///C:/Users/Admin/Desktop/z2563743987085_d64aadbdba6bbf9263e64c1f8e3dd011.jpg" alt=""></span></span></li>
                </ul>
                <p><a href="https://anhquanpro.vn/wp-content/uploads/2021/06/z2563743987085_d64aadbdba6bbf9263e64c1f8e3dd011.jpg"><img loading="lazy" class="aligncenter wp-image-7953 lazy-load-active" src="https://anhquanpro.vn/wp-content/uploads/2021/06/z2563743987085_d64aadbdba6bbf9263e64c1f8e3dd011.jpg" data-src="https://anhquanpro.vn/wp-content/uploads/2021/06/z2563743987085_d64aadbdba6bbf9263e64c1f8e3dd011.jpg" alt="Tài khoản MB bank" width="102" height="109"></a></p>
                <p><span style="font-size: 14.4px;">2. Tài Khoản BIDV chi nhánh Thái Hà :</span></p>
                <ul>
                    <li>Số tài khoản : <strong>268100242423</strong></li>
                    <li>Chủ tài khoản : Phạm Thị Ngọc .</li>
                </ul>
                <p>&nbsp;</p>
                <h3>Mọi chi tiết xin liên hệ:</h3>
                <blockquote><p>NGỌC ÁNH FURNITURE</p>
                    <p>Website : ngocanh.net – ngocanh.vn</p>
                    <p>Điện thoại : 0984 283 671 – 0989 236 671</p></blockquote>
            </div>
        </div>
    </div>  
</div>
@endsection
