
@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
    <div class="row">
        <div class="col-12">
            <div class="breadcrumb_content">
                <ul>
                    <li><a href="/">home</a></li>
                    <li><i class="fa fa-angle-right"></i></li>
                    <li>Liên hê</li>
                </ul>

            </div>
        </div>
    </div>
</div>
<!--breadcrumbs area end-->
<div>
    <h3>Công ty NGỌC ÁNH FURNITURE</h3>
    <h5>là thương hiệu nổi tiếng với nhiều năm kinh nghiệm trong việc xuất nhập khẩu nội thất đạt chuẩn quốc tế..</h5>
    <span>Địa chỉ: Số 44 Đông Ngạc-Bắc Từ Liêm-Hà Nội</span><br>
    <span>Số điện thoại: (02) 234 433 0508</span><br>
    <span>Email: noithatdep@gmail.com</span><br>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3722.5268340878647!2d105.78172771533292!3d21.091552391006733!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135aab31f7e9cef%3A0x8554ea67988d7095!2zNDQgxJAuIMSQw7RuZyBOZ-G6oWMsIMSQw7RuZyBOZ-G6oWMsIFThu6sgTGnDqm0sIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1652326346485!5m2!1svi!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

</div>
@endsection
