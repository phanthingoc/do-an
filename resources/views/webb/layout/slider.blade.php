<!--banner slider start-->
<div class="banner_slider slider_1">
    <div class="slider_active owl-carousel">
        <div class="single_slider">
            <img src="{{asset('Web1\assets/img/slider/slider_1.png')}}">
        </div>
        <div class="single_slider" style="background-image: url()">
            <img src="{{asset('Web1\assets/img/slider/slider_2.png')}}">
        </div>
        <div class="single_slider">
            <img src="{{asset('Web1\assets/img/slider/slider_3.png')}}">
        </div>
    </div>
</div>
<!--banner slider start-->
