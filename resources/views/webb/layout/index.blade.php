<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Trang chủ</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('Web1\assets\img\favicon.png') }}">

    <!-- all css here -->
    <link rel="stylesheet" href="{{ asset('Web1\assets\css\bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Web1\assets\css\plugin.css') }}">
    <link rel="stylesheet" href="{{ asset('Web1\assets\css\bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('Web1\assets\css\style.css') }}">
    <link rel="stylesheet" href="{{ asset('Web1\assets\css\responsive.css') }}">
    <script src="{{ asset('Web1\assets\js\vendor\modernizr-2.8.3.min.js') }}"></script>
</head>

<body>
    @include('sweetalert::alert')
    <!-- Add your site or application content here -->
    <!--pos page start-->
    <div class="pos_page">
        <div class="container">
            <!--pos page inner-->
            <div class="pos_page_inner">
                <!--header area -->
                <div class="header_area">
                    <!--header top-->
                    {{-- <div class="header_top">
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <div class="header_links">
                                    <ul>
                                        <li><a href="contact.html" title="Contact">Contact</a></li>



                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <!--header top end-->

                    <!--header middel-->
                    <div class="header_middel">
                        <div class="row align-items-center">
                            <!--logo start-->
                            <div class="col-lg-3 col-md-3">
                                <div class="logo">
                                    <a href="index.html"><img src="{{ asset('Web1\assets\img\logo\logo.jpg.png') }}"
                                            alt=""></a>
                                </div>
                            </div>
                            <!--logo end-->
                            <div class="col-lg-9 col-md-9">
                                <div class="header_right_info">
                                    <div class="search_bar">
                                        <form action="{{ route('search') }}" method="POST">
                                            {{ csrf_field() }}
                                            <input placeholder="Search..." name="keyword_submit" type="text">
                                            <button type="submit" name="search_items"><i
                                                    class="fa fa-search"></i></button>
                                        </form>
                                    </div>
                                    <?php
                                        $content= Cart::content()
                                    ?>
                                    <div class="shopping_cart">
                                        <a href="#"><i class="fa fa-shopping-cart"></i> <i
                                                class="fa fa-angle-down"></i></a>

                                        <!--mini cart-->
                                        <div class="mini_cart">
                                            <div class="cart_item">
                                                {{-- <div class="cart_remove">
                                                    <a title="Remove this item" href="#"><i
                                                            class="fa fa-times-circle"></i></a>
                                                </div> --}}
                                                @foreach($content as $v_content)
                                                    <div class="cart_button">
                                                        <div class="product_name">{{$v_content->name}}</div>
                                                    </div>
                                                @endforeach
                                                {{-- <div class="cart_button">
                                                    <a href="checkout.html"> Check out</a>
                                                </div> --}}
                                            </div>
                                            <!--mini cart end-->
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--header middel end-->
                        <div class="header_bottom">
                            <div class="row">
                                <div class="col-12">
                                    <div class="main_menu_inner">
                                        <div class="main_menu d-none d-lg-block">
                                            <nav>
                                                <ul>
                                                    <li class="active"><a href="/">trang chủ</a></li>
                                                    <li>
                                                        <a href="#">sản phẩm</a>
                                                        <div class="mega_menu jewelry">
                                                            <div class="mega_items jewelry">
                                                                <ul>
                                                                    @foreach ($category as $key => $cate)
                                                                        <li><a
                                                                                href="{{ route('web.danhmuc', $cate->ca_id) }}">{{ $cate->ca_name }}</a>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li><a href="{{ route('uu_dai') }}">ưu đãi</a>
                                                    </li>
                                                    <li><a href="{{ route('lien_he') }}">Liên hệ</a>
                                                        {{-- <div class="mega_menu">
                                                            <div class="mega_top fix">
                                                                <ul>
                                                                    <li><a href="portfolio.html">Chính sách bán hàng</a>
                                                                    </li>
                                                                    <li><a href="portfolio-details.html">Chính sách đổi
                                                                            trả</a></li>
                                                                    <li><a href="about.html">Chính sách bảo hành và bảo
                                                                            trì</a></li>
                                                                    <li><a href="about-2.html">Chính sách giao hàng và
                                                                            lắp đặt</a></li>
                                                                </ul>
                                                            </div>
                                                        </div> --}}
                                                    </li>
                                                    {{-- <li><a href="#">liên hệ</a></li>
                                                    <li><a href="#">Contact</a></li> --}}
                                                    <?php
                                                        $ct_id=Session::get('ct_id');
                                                        $ship_id=Session::get('ship_id');
                                                        if($ct_id!=NULL && $ship_id==Null){
                                                    ?>
                                                        <li>
                                                            <a href="{{ route('don_hang') }}" title="Đơn hàng">Đơn hàng</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ route('checkout') }}" title="My account">Thanh toán</a>
                                                        </li>
                                                    <?php
                                                    }elseif($ct_id!=null && $ship_id!=Null ){
                                                    ?>
                                                        <li>
                                                            <a href="{{ route('don_hang') }}" title="Đơn hàng">Đơn hàng</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ route('payment') }}" title="Login">Thanh toán</a>
                                                        </li>
                                                    <?php
                                                    }else{
                                                    ?>
                                                        <li>
                                                            <a href="{{ route('login_checkout') }}" title="Đơn hàng">Đơn hàng</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ route('login_checkout') }}" title="Login">Thanh toán</a>
                                                        </li>
                                                    <?php
                                                        }
                                                    ?>

                                                        <li>
                                                            <a href="{{ route('cart') }}" title="My cart">Giỏ hàng</a>
                                                        </li>
                                                    <?php
                                                        $ct_id=Session::get('ct_id');
                                                        if($ct_id!=NULL){
                                                    ?>
                                                        <li>
                                                            <a href="{{ route('logout_checkout') }}" title="Logout">Đăng xuất</a>
                                                        </li>
                                                    <?php
                                                        }else{
                                                    ?>
                                                        <li>
                                                            <a href="{{ route('login_checkout') }}" title="Login">Đăng nhập</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ route('register_checkout') }}" title="Register">Đăng ký</a>
                                                        </li>
                                                    <?php
                                                        }
                                                    ?>
                                                </ul>
                                            </nav>
                                        </div>
                                        <div class="mobile-menu d-lg-none">
                                            <nav>
                                                <ul>
                                                    <li><a href="index.html">Trang chủ</a>
                                                        <div>
                                                            <div>
                                                                <ul>
                                                                    <li><a href="index.html">Home 1</a></li>
                                                                    <li><a href="index-2.html">Home 2</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li><a href="shop.html">shop</a>
                                                        <div>
                                                            <div>
                                                                <ul>
                                                                    <li><a href="shop-list.html">shop list</a></li>
                                                                    <li><a href="shop-fullwidth.html">shop Full Width
                                                                            Grid</a></li>
                                                                    <li><a href="shop-fullwidth-list.html">shop Full
                                                                            Width list</a></li>
                                                                    <li><a href="shop-sidebar.html">shop Right
                                                                            Sidebar</a></li>
                                                                    <li><a href="shop-sidebar-list.html">shop list Right
                                                                            Sidebar</a></li>
                                                                    <li><a href="single-product.html">Product
                                                                            Details</a></li>
                                                                    <li><a href="single-product-sidebar.html">Product
                                                                            sidebar</a></li>
                                                                    <li><a href="single-product-video.html">Product
                                                                            Details video</a></li>
                                                                    <li><a href="single-product-gallery.html">Product
                                                                            Details Gallery</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li><a href="#">women</a>
                                                        <div>
                                                            <div>
                                                                <div>
                                                                    <h3><a href="#">Accessories</a></h3>
                                                                    <ul>
                                                                        <li><a href="#">Cocktai</a></li>
                                                                        <li><a href="#">day</a></li>
                                                                        <li><a href="#">Evening</a></li>
                                                                        <li><a href="#">Sundresses</a></li>
                                                                        <li><a href="#">Belts</a></li>
                                                                        <li><a href="#">Sweets</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div>
                                                                    <h3><a href="#">HandBags</a></h3>
                                                                    <ul>
                                                                        <li><a href="#">Accessories</a></li>
                                                                        <li><a href="#">Hats and Gloves</a></li>
                                                                        <li><a href="#">Lifestyle</a></li>
                                                                        <li><a href="#">Bras</a></li>
                                                                        <li><a href="#">Scarves</a></li>
                                                                        <li><a href="#">Small Leathers</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div>
                                                                    <h3><a href="#">Tops</a></h3>
                                                                    <ul>
                                                                        <li><a href="#">Evening</a></li>
                                                                        <li><a href="#">Long Sleeved</a></li>
                                                                        <li><a href="#">Shrot Sleeved</a></li>
                                                                        <li><a href="#">Tanks and Camis</a></li>
                                                                        <li><a href="#">Sleeveless</a></li>
                                                                        <li><a href="#">Sleeveless</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div>
                                                                    <a href="#"><img
                                                                            src="{{ asset('Web1\assets\img\banner\banner1.jpg') }}"
                                                                            alt=""></a>
                                                                </div>
                                                                <div>
                                                                    <a href="#"><img
                                                                            src="{{ asset('Web1\assets\img\banner\banner2.jpg') }}"
                                                                            alt=""></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li><a href="#">men</a>
                                                        <div>
                                                            <div>
                                                                <div>
                                                                    <h3><a href="#">Rings</a></h3>
                                                                    <ul>
                                                                        <li><a href="#">Platinum Rings</a></li>
                                                                        <li><a href="#">Gold Ring</a></li>
                                                                        <li><a href="#">Silver Ring</a></li>
                                                                        <li><a href="#">Tungsten Ring</a></li>
                                                                        <li><a href="#">Sweets</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div>
                                                                    <h3><a href="#">Bands</a></h3>
                                                                    <ul>
                                                                        <li><a href="#">Platinum Bands</a></li>
                                                                        <li><a href="#">Gold Bands</a></li>
                                                                        <li><a href="#">Silver Bands</a></li>
                                                                        <li><a href="#">Silver Bands</a></li>
                                                                        <li><a href="#">Sweets</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div>
                                                                    <a href="#"><img
                                                                            src="{{ asset('Web1\assets\img\banner\banner3.jpg') }}"
                                                                            alt=""></a>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </li>
                                                    <li><a href="#">pages</a>
                                                        <div>
                                                            <div>
                                                                <div>
                                                                    <h3><a href="#">Column1</a></h3>
                                                                    <ul>
                                                                        <li><a href="portfolio.html">Portfolio</a></li>
                                                                        <li><a href="portfolio-details.html">single
                                                                                portfolio </a></li>
                                                                        <li><a href="about.html">About Us </a></li>
                                                                        <li><a href="about-2.html">About Us 2</a></li>
                                                                        <li><a href="services.html">Service </a></li>
                                                                        <li><a href="my-account.html">my account </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div>
                                                                    <h3><a href="#">Column2</a></h3>
                                                                    <ul>
                                                                        <li><a href="blog.html">Blog </a></li>
                                                                        <li><a href="blog-details.html">Blog Details
                                                                            </a></li>
                                                                        <li><a href="blog-fullwidth.html">Blog
                                                                                FullWidth</a></li>
                                                                        <li><a href="blog-sidebar.html">Blog Sidebar</a>
                                                                        </li>
                                                                        <li><a href="faq.html">Frequently Questions</a>
                                                                        </li>
                                                                        <li><a href="404.html">404</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div>
                                                                    <h3><a href="#">Column3</a></h3>
                                                                    <ul>
                                                                        <li><a href="contact.html">Contact</a></li>
                                                                        <li><a href="cart.html">cart</a></li>
                                                                        <li><a href="checkout.html">Checkout </a></li>
                                                                        <li><a href="wishlist.html">Wishlist</a></li>
                                                                        <li><a href="login.html">Login</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li><a href="blog.html">blog</a>
                                                        <div>
                                                            <div>
                                                                <ul>
                                                                    <li><a href="blog-details.html">blog details</a>
                                                                    </li>
                                                                    <li><a href="blog-fullwidth.html">blog fullwidth</a>
                                                                    </li>
                                                                    <li><a href="blog-sidebar.html">blog sidebar</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li><a href="contact.html">contact us</a></li>

                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--header end -->

                    <!--pos home section-->
                    <div class=" pos_home_section">
                        <div class="row pos_home">

                            @include('webb.layout.sliderbar')
                            <div class="col-lg-9 col-md-12">
                                @include('webb.layout.slider')
                                @yield('content')
                            </div>
                        </div>
                    </div>
                    <!--pos home section end-->
                </div>
                <!--pos page inner end-->
            </div>
        </div>
        <!--pos page end-->

        <!--footer area start-->
        <div class="footer_area">
            <div class="footer_top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="footer_widget">
                                <h3>Ngọc Ánh Furniture</h3>
                                <p>là thương hiệu nổi tiếng với nhiều năm kinh nghiệm trong việc xuất nhập khẩu nội thất
                                    đạt chuẩn quốc tế..</p>
                                <div class="footer_widget_contect">
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i>Số 44 Đông Ngạc-Bắc Từ Liêm-Hà
                                        Nội</p>

                                    <p><i class="fa fa-mobile" aria-hidden="true"></i> (02) 234 433 0508</p>
                                    <a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i> noithatdep@gmail.com
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="footer_widget">
                                <h3>Về chúng tôi</h3>
                                <ul>
                                    <li><a href="{{ route('gioithieu') }}">Giới thiệu</a></li>
                                    <li><a href="{{ route('tintuc') }}">Tin tức</a></li>
                                    <li><a href="{{ route('lien_he')}}">Liên hệ</a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="footer_widget">
                                <h3>Hỗ trợ khách hàng</h3>
                                <ul>
                                    <li><a href="{{ route('giaohang') }}">Phương thức giao hàng</a></li>
                                    <li><a href="{{ route('thanhtoan') }}">Phương thức thanh toán </a></li>
                                    <li><a href="{{ route('ordering_guide') }}">Hướng dẫn đặt hàng</a></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_bottom">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6">
                            <div class="copyright_area">
                                <ul>
                                    <li><a href="#"> about us </a></li>
                                    <li><a href="#"> Customer Service </a></li>
                                    <li><a href="#"> Privacy Policy </a></li>
                                </ul>
                                <p>Copyright &copy; 2018 <a href="#">Ngọc Ánh Furniture</a>. All rights reserved. </p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="footer_social text-right">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                    <li><a class="pinterest" href="#"><i class="fa fa-pinterest-p"
                                                aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-wifi" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--footer area end-->

        <!-- modal area start -->
        <div class="modal fade" id="modal_box" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="modal_body">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-12">
                                    <div class="modal_tab">
                                        <div class="tab-content" id="pills-tabContent">
                                            <div class="tab-pane fade show active" id="tab1" role="tabpanel">
                                                <div class="modal_tab_img">
                                                    <a href="#"><img src="assets\img\product\product13.jpg" alt=""></a>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab2" role="tabpanel">
                                                <div class="modal_tab_img">
                                                    <a href="#"><img src="assets\img\product\product14.jpg" alt=""></a>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab3" role="tabpanel">
                                                <div class="modal_tab_img">
                                                    <a href="#"><img src="assets\img\product\product15.jpg" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal_tab_button">
                                            <ul class="nav product_navactive" role="tablist">
                                                <li>
                                                    <a class="nav-link active" data-toggle="tab" href="#tab1" role="tab"
                                                        aria-controls="tab1" aria-selected="false"><img
                                                            src="assets\img\cart\cart17.jpg" alt=""></a>
                                                </li>
                                                <li>
                                                    <a class="nav-link" data-toggle="tab" href="#tab2" role="tab"
                                                        aria-controls="tab2" aria-selected="false"><img
                                                            src="assets\img\cart\cart18.jpg" alt=""></a>
                                                </li>
                                                <li>
                                                    <a class="nav-link button_three" data-toggle="tab" href="#tab3"
                                                        role="tab" aria-controls="tab3" aria-selected="false"><img
                                                            src="assets\img\cart\cart19.jpg" alt=""></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-12">
                                    <div class="modal_right">
                                        <div class="modal_title mb-10">
                                            <h2>Handbag feugiat</h2>
                                        </div>
                                        <div class="modal_price mb-10">
                                            <span class="new_price">$64.99</span>
                                            <span class="old_price">$78.99</span>
                                        </div>
                                        <div class="modal_content mb-10">
                                            <p>Short-sleeved blouse with feminine draped sleeve detail.</p>
                                        </div>
                                        <div class="modal_size mb-15">
                                            <h2>size</h2>
                                            <ul>
                                                <li><a href="#">s</a></li>
                                                <li><a href="#">m</a></li>
                                                <li><a href="#">l</a></li>
                                                <li><a href="#">xl</a></li>
                                                <li><a href="#">xxl</a></li>
                                            </ul>
                                        </div>
                                        <div class="modal_add_to_cart mb-15">
                                            <form action="#">
                                                <input min="0" max="100" step="2" value="1" type="number">
                                                <button type="submit">add to cart</button>
                                            </form>
                                        </div>
                                        <div class="modal_description mb-15">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                                veniam,</p>
                                        </div>
                                        <div class="modal_social">
                                            <h2>Share this product</h2>
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- modal area end -->




        <!-- all js here -->
        <script src="{{ asset('Web1\assets\js\vendor\jquery-1.12.0.min.js') }}"></script>
        <script src="{{ asset('Web1\assets\js\popper.js') }}"></script>
        <script src="{{ asset('Web1\assets\js\bootstrap.min.js') }}"></script>
        <script src="{{ asset('Web1\assets\js\ajax-mail.js') }}"></script>
        <script src="{{ asset('Web1\assets\js\plugins.js') }}"></script>
        <script src="{{ asset('Web1\assets\js\main.js') }}"></script>
        
        @yield('js')
</body>

</html>
