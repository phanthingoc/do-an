<div class="col-lg-3 col-md-8 col-12">
    <!--sidebar banner-->
    <div class="sidebar_widget banner mb-35">
        <div class="banner_img mb-35">
            <a href="#"><img src="{{asset('Web1\assets\img\banner\banner5.jpg')}}" alt=""></a>
        </div>
        <div class="banner_img">
            <a href="#"><img src="{{asset('Web1\assets\img\banner\banner6.jpg')}}" alt=""></a>
        </div>
    </div>
    <!--sidebar banner end-->

    <!--categorie menu start-->
    <div class="sidebar_widget1 catrgorie mb-35">
        <h3>Thể loại</h3>
        <ul>
            @foreach($category as $key => $cate)
            <li class="has-sub">
                <a href="{{route('web.danhmuc',$cate->ca_id)}}"><i class="fa fa-caret-right"></i>{{$cate->ca_name}}</a>
                {{-- <ul class="categorie_sub">
                    <li><a href="{{route('web.danhmuc',$cate->ca_id)}}"><i class="fa fa-caret-right"></i> {{$cate->ca_name}}</a></li>
                </ul> --}}
            </li>
            @endforeach
        </ul>
    </div>
    <!--categorie menu end-->

   
    <!--popular tags end-->

    <!--sidebar banner-->
    <div class="sidebar_widget bottom ">
        <div class="banner_img">
            <a href="#"><img src="{{asset('Web1\assets\img\banner\banner9.jpg')}}" alt=""></a>
        </div>
    </div>
    <!--sidebar banner end-->



</div>
