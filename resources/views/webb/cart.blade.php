
@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
    <div class="row">
        <div class="col-12">
            <div class="breadcrumb_content">
                <ul>
                    <li><a href="/">home</a></li>
                    <li><i class="fa fa-angle-right"></i></li>
                    <li>Giỏ hàng</li>
                </ul>

            </div>
        </div>
    </div>
</div>
<!--breadcrumbs area end-->



<!--shopping cart area start -->
<div class="shopping_cart_area">
    <div>
        <div class="row">
            <div class="col-12">
                <div class="table_desc">
                    <div class="cart_page table-responsive">
                        <?php
                        $content= Cart::content()
                        ?>
                        <table>
                            <thead>
                            <tr>
                                <th class="product_remove">Delete</th>
                                <th class="product_thumb">Image</th>
                                <th class="product_name">Sản phẩm</th>
                                <th class="product-price">Đơn giá</th>
                                <th class="product_quantity">Số lượng</th>
                                <th class="product_total">Thành tiền</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($content as $v_content)
                            <tr>
                                <td class="product_remove"><a href="{{route('deletecart',$v_content->rowId)}}"><i class="fa fa-trash-o"></i></a></td>
                                <td class="product_thumb"><a href="#"><img src="{{asset('upload/sanpham/'.$v_content->options->image)}}" alt="" style=" width:80px"></a></td>
                                <td class="product_name"><a href="#">{{$v_content->name}}</a></td>
                                <td class="product-price">{{number_format($v_content->price)}} đ</td>
                                <td class="product_quantity">
                                    <form action="{{ route('update.cart') }}" method="Post">
                                        {{csrf_field()}}
                                        <input type="hidden" value="{{$v_content->rowId}}" name="rowIdcart" class="form-control">
                                        <input min="0" max="100" type="number" name="quantityc" value="{{$v_content->qty}}">
                                        <button type="submit" value="Update" name="qty_name" class="btn btn-default btn-sm">Update</button>
                                    </form>
                                </td>
                                <td class="product_total">
                                        <?php
                                        $subtotal = $v_content->price * $v_content->qty;
                                        echo number_format($subtotal)
                                        ?>đ
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!--coupon code area start-->
        <div class="coupon_area">
                <div class="col-lg-6 col-md-6">
                    <div class="coupon_code">
                        <div class="coupon_inner">
                            <div class="cart_subtotal">
                                <p>Tổng tiền</p>
                                <p class="cart_amount">{{Cart::subtotal(0,',','.')}} đ</p>
                            </div>
                            <div class="cart_subtotal ">
                                <p>Phí vận chuyển</p>
                                <p class="cart_amount">Free</p>
                            </div>
                            <div class="cart_subtotal">
                                <p>Tổng thanh toán</p>
                                <p class="cart_amount">{{Cart::total(0,',','.')}} đ</p>
                            </div>
                            <div class="checkout_btn">

                                <?php
                                $ct_id=Session::get('ct_id');
                                if($ct_id!=NULL){

                                ?>
                                <li><a href="{{route('checkout')}}">Thanh toán</a></li>
                                <?php
                                }else{

                                ?>
                                <li><a href="{{route('login_checkout')}}">Thanh toán</a></li>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <!--coupon code area end-->
    </div>
</div>
<!--shopping cart area end -->
@endsection
