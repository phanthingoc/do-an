
@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
    <div class="row">
        <div class="col-12">
            <div class="breadcrumb_content">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><i class="fa fa-angle-right"></i></li>
                    @foreach($detail_sanpham as $key => $sanpham)
                    <li>{{$sanpham->b_name}}</li>
                    @endforeach
                </ul>

            </div>
        </div>
    </div>
</div>
<!--breadcrumbs area end-->

<!--product wrapper start-->
<div class="product_details">
    <div class="row">
        @foreach($detail_sanpham as $key =>$value)
        <div class="col-lg-5 col-md-6">
            <div class="product_tab fix">
                <div class="product_tab_button">
                    <ul class="nav" role="tablist">
                        <li>
                            <a class="active" data-toggle="tab" href="#p_tab1" role="tab" aria-controls="p_tab1" aria-selected="false"><img src="{{asset('upload/sach/'.$value->b_image)}}" alt=""></a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content produc_tab_c">
                    <div class="tab-pane fade show active" id="p_tab1" role="tabpanel">
                        <div class="modal_img">
                            <a href="#"><img src="{{asset('upload/sanpham/'.$value->b_image)}}" alt=""></a>
                            <div class="img_icone">
                                <img src="{{asset('upload/sanpham/'.$value->b_image)}}" alt="">
                            </div>
                            <div class="view_img">
                                <a class="large_view" href="{{asset('upload/sanpham/'.$value->b_image)}}"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="p_tab2" role="tabpanel">
                        <div class="modal_img">
                            <a href="#"><img src="assets\img\product\product14.jpg" alt=""></a>
                            <div class="img_icone">
                                <img src="assets\img\cart\span-new.png" alt="">
                            </div>
                            <div class="view_img">
                                <a class="large_view" href="assets\img\product\product14.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="p_tab3" role="tabpanel">
                        <div class="modal_img">
                            <a href="#"><img src="assets\img\product\product15.jpg" alt=""></a>
                            <div class="img_icone">
                                <img src="assets\img\cart\span-new.png" alt="">
                            </div>
                            <div class="view_img">
                                <a class="large_view" href="assets\img\product\product15.jpg"> <i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-lg-7 col-md-6">
            <div class="product_d_right">
                <h1>{{$value->b_name}}</h1>
                <div class="product_ratting mb-10">
                    <ul>
                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                    </ul>
                </div>

                <div class="content_price mb-15">
                    <span>{{number_format($value->gia)}} đ</span>
                </div>

                <div class="box_quantity mb-20">
                    <form action="{{route('web.cart')}} " method="POST">
                        {{csrf_field()}}
                        <span>
                        <label>Số lượng</label>
                        <input name="qty" type="number" value="1" />
                        <input name="productid_hidden" type="hidden" value="{{$value->b_id}}" />
                        <button type="submit"><i class="fa fa-shopping-cart"></i> Add to cart</button>
                        </span>
                    </form>

                </div>

            </div>
        </div>
        @endforeach
    </div>
</div>
<!--product details end-->


<!--product info start-->
<div class="product_d_info">
    <div class="row">
        @foreach($detail_sanpham as $key =>$value)
        <div class="col-12">
            <div class="product_d_inner">
                <div class="product_info_button">
                    <ul class="nav" role="tablist">
                        <li>
                            <a class="active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">Mô tả</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#sheet" role="tab" aria-controls="sheet" aria-selected="false">Chi tiết</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="info" role="tabpanel">
                        <div class="product_info_content">
                            <p>{{$value->mota}}</p>
                    </div>
                    </div>
                    <div class="tab-pane fade" id="sheet" role="tabpanel">
                        <div class="product_d_table">
                            <form action="#">
                                <table>
                                    <tbody>


                                    <tr>
                                        <td class="first_child">Nhà cung cấp</td>
                                        <td>{{$value->ncc_name}}</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </form>
                        </div>
                        <div class="product_info_content">
                            <p>{{$value->mota}}</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
        @endforeach
</div>
<!--product info end-->


<!--new product area start-->
<div class="new_product_area product_page">
    <div class="row">
        <div class="col-12">
            <div class="block_title">
                <h3>  Sản phẩm tương tự</h3>
            </div>
        </div>
    </div>
    <div class="row">
        @foreach($splienquan as $key => $lienqua)


            <div class="col-lg-3 col-md-4 col-sm-6">

                <div class="single_product">
                    <div class="product_thumb">
                        <a href="{{route('web.chitietsp',$lienqua->b_id)}}"><img src="{{asset('upload/sanpham/'.$lienqua->b_image)}}" alt=""></a>
                        <div class="img_icone">
                            <img src="{{asset('upload/sanpham/'.$lienqua->b_image)}}" alt="">
                        </div>
                        <div class="product_action">
                            <a href="#"> <i class="fa fa-shopping-cart"></i> Add to cart</a>
                        </div>
                    </div>
                    <div class="product_content">
                        <span class="product_price">{{number_format($lienqua->gia)}}</span>
                        <h3 class="product_title"><a href="{{route('web.chitietsp',$lienqua->b_id)}}">{{$lienqua->b_name}}</a></h3>
                    </div>
                    <div class="product_info">
                        <ul>
                            <li><a href="{{route('web.chitietsp',$lienqua->b_id)}}" title="Quick view">Chi tiết</a></li>
                        </ul>
                    </div>
                </div>

            </div>
            @endforeach


    </div>
</div>
<!--new product area start-->
@endsection
