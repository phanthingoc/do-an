@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
    <div class="block_title">
        @foreach($ca_name as $key => $cate)
        <h3>{{$cate-> ca_name}}</h3>
        @endforeach
    </div>
    <div class="row">
        @foreach($cate_by_id as $key => $cate)
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="single_product">
                    <div class="product_thumb">
                        <a href="{{route('web.chitietsp',$cate->b_id)}}"><img src="{{asset('upload/sanpham/'.$cate->b_image)}}" alt=""></a>
                        <div class="img_icone">
                            <img src="{{asset('upload/sach/'.$cate->b_image)}}" alt="">
                            @if($cate->sale != 0)
                            <div class="img_icone" style="background-color: red; color:white">
                                -{{ $cate->sale }}%
                           </div>
                           @endif
                        </div>
                        <div class="product_action">
                            <a href="{{ route('addToCart',$cate->b_id) }}"> <i class="fa fa-shopping-cart"></i> Add to cart</a>
                        </div>
                    </div>
                    <div class="product_content">
                        <div class="content_price mb-15">
                            <span>{{ number_format( $cate->gia - $cate->gia*($cate->sale / 100)) }} đ</span>
                            @if($cate->sale != 0)
                            <span class="old-price">{{number_format( $cate->gia)}} đ</span>
                            @endif
                        </div>
                        <h3 class="product_title"><a href="{{route('web.chitietsp',$cate->b_id)}}">{{$cate->b_name}}</a></h3>
                    </div>
                    <div class="product_info">
                        <ul>
                            <li><a href="{{route('web.chitietsp',$cate->b_id)}}" data-toggle="modal" data-target="#modal_box" title="Quick view">Chi tiết</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection
