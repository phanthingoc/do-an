
@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
    <div class="row">
        <div class="col-12">
            <div class="breadcrumb_content">
                <ul>
                    <li><a href="/">home</a></li>
                    <li><i class="fa fa-angle-right"></i></li>
                    <li>Hướng dẫn đặt hàng</li>
                </ul>

            </div>
        </div>
    </div>
</div>
<div class="faq_content_area">
    <div class="row">
        <div class="col-12">
            <div class="faq_content_wrapper">
                <h4>Hướng dẫn đặt hàng</h4>
                <p>Bạn có thể mua sắm dễ dàng tại đây với các bước đặt hàng đơn giản như sau:</p>
                <span>1. Chọn sản phẩm</span><br>
                <span>2. Thêm vào giỏ hàng/ Mua hàng</span><br>
                <span>3. Xem giỏ hàng</span><br>
                <span>4. Thanh toán</span><br>
                <span>5. Đăng nhập</span><br>
                <span>6. Nhập thông tin cần thiết</span><br>
                <span>7. Đặt hàng</span><br>
                <span>Địa chỉ: Số 44 Đông Ngạc-Bắc Từ Liêm-Hà Nội</span><br>
                <span>Số điện thoại: (02) 234 433 0508</span><br>
                <span>Email: noithatdep@gmail.com</span><br>
            </div>
        </div>
    </div>  
</div>
@endsection
