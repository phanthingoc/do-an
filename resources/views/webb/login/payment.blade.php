@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="/">Trang chủ</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Thanh toán giỏ hàng</li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->
<div class="col-lg-6 col-md-6">
                    <?php
                    $content= Cart::content()
                    ?>
<form action="#">
    <h3>Đơn hàng của bạn</h3>
    <div class="order_table table-responsive mb-30">
        <table>
            <thead>
            <tr>
                <th>Sản phẩm</th>
                <th>Đơn giá</th>
                <th>Thành tiền</th>
            </tr>
            </thead>
            <tbody>
            @foreach($content as $v_content)
                <tr>
                    <td> {{$v_content->name}}<strong> ×{{$v_content->qty}}</strong></td>
                    <td>{{number_format($v_content->price)}} đ</td>
                    <td>
                        <?php
                        $subtotal = $v_content->price * $v_content->qty;
                        echo number_format($subtotal)
                        ?>đ
                    </td>
                </tr>
            </tbody>
            <tfoot>
            <tr>
                <th>Tổng tiền</th>
                <td>{{Cart::subtotal(0,',','.')}} đ</td>
            </tr>
            <tr>
                <th>Phí vận chuyển</th>
                <td><strong><b><i style="color:#FF0040"><u>Miễn phí</u></i></b> vận chuyển</strong></td>
            </tr>
            <tr class="order_total">
                <th>Tổng thanh toán</th>
                <td><strong>{{Cart::total(0,',','.')}} đ</strong></td>
            </tr>
            </tfoot>
            @endforeach
        </table>
    </div>
    <div >
        <h5>Hình thức thanh toán</h5>
    </div>
    <div class="payment_method">
        <div class="panel-default">
            <input id="payment_defult" name="check_method" value="payment" type="radio" data-target="createp_account">
            <label for="payment_defult" data-toggle="collapse" data-target="#collapsedefult" aria-controls="collapsedefult">Thanh toán Payment <img src="{{'Web1\assets\img\visha\papyel.png'}}" alt=""></label>

            <div id="collapsedefult" class="collapse one" data-parent="#accordion">
                <div class="card-body1">
                    <p>Thanh toán qua PayPal; bạn có thể thanh toán bằng thẻ tín dụng của mình nếu bạn không có tài khoản PayPal.</p>
                </div>
            </div>
        </div>
        <div class="panel-default">
            <input id="payment_defult" name="check_method" value="tien" type="radio" data-target="createp_account">
            <label for="payment_defult" data-toggle="collapse" data-target="#collapsedefult" aria-controls="collapsedefult">Thanh toán khi nhận hàng</label>

            <div id="collapsedefult" class="collapse one" data-parent="#accordion">
                <div class="card-body1">
                    <p>Bạn cần thanh toán tiền hàng cho shipper.</p>
                </div>
            </div>
        </div>
        <div class="order_button">
            <button type="submit">Thanh toán</button>
        </div>
    </div>
</form>
</div>
@endsection
