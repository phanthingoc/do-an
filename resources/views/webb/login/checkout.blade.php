@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="/">Trang chủ</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Thanh toán giỏ hàng</li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->


    <!--Checkout page section-->
    <div class="Checkout_section">
        <div class="row">
            <div class="col-12">
                <div class="user-actions mb-20">
                    <h3>
                        <i class="fa fa-file-o" aria-hidden="true"></i>
                        Returning customer?
                        <a class="Returning" href="#" data-toggle="collapse" data-target="#checkout_login" aria-expanded="true">Click here to login</a>

                    </h3>
                    <div id="checkout_login" class="collapse" data-parent="#accordion">
                        <div class="checkout_info">
                            <p>If you have shopped with us before, please enter your details in the boxes below. If you are a new customer please proceed to the Billing & Shipping section.</p>
                            <form action="#">
                                <div class="form_group mb-20">
                                    <label>Username or email <span>*</span></label>
                                    <input type="text">
                                </div>
                                <div class="form_group mb-25">
                                    <label>Password  <span>*</span></label>
                                    <input type="text">
                                </div>
                                <div class="form_group group_3 ">
                                    <input value="Login" type="submit">
                                    <label for="remember_box">
                                        <input id="remember_box" type="checkbox">
                                        <span> Remember me </span>
                                    </label>
                                </div>
                                <a href="#">Lost your password?</a>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="user-actions mb-20">
                    <h3>
                        <i class="fa fa-file-o" aria-hidden="true"></i>
                        Returning customer?
                        <a class="Returning" href="#" data-toggle="collapse" data-target="#checkout_coupon" aria-expanded="true">Click here to enter your code</a>

                    </h3>
                    <div id="checkout_coupon" class="collapse" data-parent="#accordion">
                        <div class="checkout_info">
                            <form action="#">
                                <input placeholder="Coupon code" type="text">
                                <input value="Apply coupon" type="submit">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="checkout_form">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <form action="{{route('checkout.customer')}}" method="POST">
                        {{csrf_field()}}
                        <h3>Thông tin khách hàng</h3>
                        <div class="row">

                            <div class="col-12 mb-30">
                                <label>Họ tên <span>*</span></label>
                                <input type="text" name="ship_name">
                            </div>
                            <div class="col-12 mb-30">
                                <label>Địa chỉ <span>*</span></label>
                                <input type="text" placeholder="Số nhà, tên đường, phường,..."  name="ship_address">
                            </div>
                            <div class="col-lg-6 mb-30">
                                <label>Phone<span>*</span></label>
                                <input type="text" name="ship_phone">

                            </div>
                            <div class="col-lg-6 mb-30">
                                <label> Email Address   <span>*</span></label>
                                <input type="text" name="ship_email">
                            </div>
                            <div>
                                <input type="submit" value="Xác nhận" name="send_order" class="btn btn-outline-info btn-sm">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6 col-md-6">
                    <?php
                    $content= Cart::content()
                    ?>
                    <form action="{{route('order')}}" method="POST">
                        {{csrf_field()}}
                        <h3>Đơn hàng của bạn</h3>
                        <div class="order_table table-responsive mb-30">
                            <table>
                                <thead>
                                <tr>
                                    <th>Sản phẩm</th>
                                    <th>Đơn giá</th>
                                    <th>Thành tiền</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($content as $v_content)
                                <tr>
                                    <td> {{$v_content->name}}<strong> ×{{$v_content->qty}}</strong></td>
                                    <td>{{number_format($v_content->price)}} đ</td>
                                    <td>
                                        <?php
                                        $subtotal = $v_content->price * $v_content->qty;
                                        echo number_format($subtotal)
                                        ?>đ
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Tổng tiền</th>
                                    <td>{{Cart::subtotal(0,',','.')}} đ</td>
                                </tr>
                                <tr>
                                    <th>Phí vận chuyển</th>
                                    <td><strong><b><i style="color:#FF0040"><u>Miễn phí</u></i></b> vận chuyển</strong></td>
                                </tr>
                                <tr class="order_total">
                                    <th>Tổng thanh toán</th>
                                    <td><strong>{{Cart::total(0,',','.')}} đ</strong></td>
                                </tr>
                                </tfoot>
                                @endforeach
                            </table>
                        </div>
                        <div >
                            <h5>Hình thức thanh toán</h5>
                        </div>
                        <div class="payment_method">
                            {{-- <div class="panel-default">
                                <input id="payment_defult" name="check_method" value="1" type="radio" data-target="createp_account">
                                <label for="payment_defult" data-toggle="collapse" data-target="#collapsedefult" aria-controls="collapsedefult">Thanh toán Payment <img src="{{'Web1\assets\img\visha\papyel.png'}}" alt=""></label>

                                <div id="collapsedefult" class="collapse one" data-parent="#accordion">
                                    <div class="card-body1">
                                        <p>Thanh toán qua PayPal; bạn có thể thanh toán bằng thẻ tín dụng của mình nếu bạn không có tài khoản PayPal.</p>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="panel-default">
                                <input id="payment_defult" name="check_method" value="3" type="radio" data-target="createp_account">
                                <label for="payment_defult" data-toggle="collapse" data-target="#collapsedefult" aria-controls="collapsedefult">Thanh toán Momo</label>

                                {{-- <div id="collapsedefult" class="collapse one" data-parent="#accordion">
                                    <div class="card-body1">
                                        <p>Thanh toán qua PayPal; bạn có thể thanh toán bằng thẻ tín dụng của mình nếu bạn không có tài khoản PayPal.</p>
                                    </div>
                                </div> --}}
                            </div>
                            <div class="panel-default">
                                <input id="payment_defult" name="check_method" value="1" type="radio" data-target="createp_account">
                                <label for="payment_defult" data-toggle="collapse" data-target="#collapsedefult" aria-controls="collapsedefult">Thanh toán khi nhận hàng</label>

                                <div id="collapsedefult" class="collapse one" data-parent="#accordion">
                                    <div class="card-body1">
                                        <p>Bạn cần thanh toán tiền hàng cho shipper.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="order_button">
                                <button type="submit" name="payUrl">Thanh toán</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--Checkout page section end-->


@endsection
