@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="/">Trang chủ</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Đăng nhập</li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!-- customer login start -->
    <div class="customer_login">
        <div class="row">
            <!--login area start-->
            <div class="col-lg-6 col-md-6">
                <div class="account_form">
                    <h2>Đăng nhập</h2>
                    <form action="{{route('login')}}" method="POST">
                        {{csrf_field()}}
                        <p>
                            <label>Email <span>*</span></label>
                            <input type="text" name="email_account">
                        </p>
                        <p>
                            <label>Mật khẩu <span>*</span></label>
                            <input type="password" name="password_account">
                        </p>
                        <div class="login_submit">
                            <button type="submit">Đăng nhập</button>
                            <label for="remember">
                                <input id="remember" type="checkbox">
                                Nhớ tôi
                            </label>
                        </div>

                    </form>
                </div>
            </div>
            <!--login area start-->
        </div>
    </div>
    <!-- customer login end -->



@endsection
