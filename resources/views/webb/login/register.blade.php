@extends('webb.layout.index')

@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="/">Trang chủ</a></li>
                        <li><i class="fa fa-angle-right"></i></li>
                        <li>Đăng ký</li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!-- customer login start -->
    <div class="customer_login">
        <div class="row">
            <!--register area start-->
            <div class="col-lg-6 col-md-6">
                <div class="account_form register">
                    <h2>Đăng ký</h2>
                    <form action="{{route('add.customer')}}" method="POST">
                        {{csrf_field()}}
                        <p>
                            <label>UserName <span>*</span></label>
                            <input type="text" name="ct_name">
                        </p>
                        <p>
                            <label>Email <span>*</span></label>
                            <input type="text" name="email">
                        </p>
                        <p>
                            <label>Số điện thoại  <span>*</span></label>
                            <input type="text" name="sdt">
                        </p>
                        <p>
                            <label>Mật khẩu <span>*</span></label>
                            <input type="password" name="password">
                        </p>
                        <div class="login_submit">
                            <button type="submit">Đăng ký</button>
                        </div>
                    </form>
                </div>
            </div>
            <!--register area end-->
        </div>
    </div>
    <!-- customer login end -->



@endsection
