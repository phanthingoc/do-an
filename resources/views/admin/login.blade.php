<!DOCTYPE html>
<head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Visitors Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- bootstrap-css -->
    <link rel="stylesheet" href="{{asset('Admin/web/css/bootstrap.min.css')}}" >
    <!-- //bootstrap-css -->
    <!-- Custom CSS -->
    <link href="{{asset('Admin/web/css/style.css')}}" rel='stylesheet' type='text/css' />
    <link href="{{asset('Admin/web/css/style-responsive.css')}}" rel="stylesheet"/>
    <!-- font CSS -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- font-awesome icons -->
    <link rel="stylesheet" href="{{asset('Admin/web/css/font.css')}}" type="text/css"/>
    <link href="{{asset('Admin/web/css/font-awesome.css')}}" rel="stylesheet">
    <!-- //font-awesome icons -->
    <script src="{{asset('Admin/web/js/jquery2.0.3.min.js')}}"></script>
</head>
{{-- <!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Đăng nhập</title>
  <!-- base:css -->
  <link rel="stylesheet" href="{{asset('Admin/template/vendors/typicons.font/font/typicons.css')}}">
  <link rel="stylesheet" href="{{asset('Admin/template/vendors/css/vendor.bundle.base.css')}}">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{asset('Admin/template/css/vertical-layout-light/style.css')}}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('Admin/template/images/favicon.png')}}" />
</head> --}}
<body>
<div class="log-w3">
    <div class="w3layouts-main">
        <h2>Đăng nhập</h2>
        <form action="{{route('admin.loginPost')}}" method="post">
            {{csrf_field()}}
            <input type="email" class="ggg" name="email" placeholder="Nhập email để đăng nhập" required="">
            <input type="password" class="ggg" name="password" placeholder="Nhập password" required="">
            <span><input type="checkbox" />Nhớ</span>
            <h6><a href="#">Quên mật khẩu</a></h6>
            <div class="clearfix"></div>
            <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">ĐĂNG NHẬP</button>

            {{-- <button type="submit" value="Sign In" name="login">Đăng nhập</button> --}}
        </form>
        <p><a href="{{ route('admin.register') }}">Đăng kí</a></p>
    </div>
</div>
{{-- <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <h4>Xin chào! Bắt đầu nào</h4>
              <h6 class="font-weight-light">Đăng nhập để tiếp tục.</h6>
              <form class="pt-3" action="{{route('admin.loginPost')}}" method="post">
                @csrf
                <div class="form-group">
                  <input type="email" name="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Username">
                </div>
                <div class="form-group">
                  <input type="password" name="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Mật khẩu">
                </div>
                <div class="mt-3">
                  <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">ĐĂNG NHẬP</button>
                </div> --}}
                {{-- <div class="my-2 d-flex justify-content-between align-items-center">
                  <div class="form-check">
                    <label class="form-check-label text-muted">
                      <input type="checkbox" class="form-check-input">
                      Keep me signed in
                    </label>
                  </div>
                  <a href="#" class="auth-link text-black">Forgot password?</a>
                </div> --}}
                {{-- <div class="mb-2">
                  <button type="button" class="btn btn-block btn-facebook auth-form-btn">
                    <i class="typcn typcn-social-facebook-circular mr-2"></i>Connect using facebook
                  </button>
                </div> --}}
                {{-- <div class="text-center mt-4 font-weight-light">
                  Không có tài khoản? <a href="{{ route('admin.register') }}" class="text-primary">Đăng ký</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div> --}}
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- base:js -->
  <script src="{{asset('Admin/template/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="{{asset('Admin/template/js/off-canvas.js')}}"></script>
  <script src="{{asset('Admin/template/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('Admin/template/js/template.js')}}"></script>
  <script src="{{asset('Admin/template/js/settings.js')}}"></script>
  <script src="{{asset('Admin/template/js/todolist.js')}}"></script>
  <!-- endinject -->
</body>

</html>

