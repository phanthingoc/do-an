@extends('admin.layout.admin')
@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold">CỬA HÀNG NỘI THẤT</h3>
            </div>
        </div><br>
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h4> Danh sách thể loại</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="/admin/home">Dashboard </a></li>
                                    <li>/</li>
                                    <li><a href="/admin/category"> Thể loại</a></li>
                                    <li>/</li>
                                    <li class="active"> Danh sách</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Danh sách</strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Tên</th>
                                        <th>Delete</th>
                                        <th>Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($category as $item)
                                        <tr class="even gradeC">
                                            <td>{{$item['ca_id']}}</td>
                                            <td>{{$item['ca_name']}}</td>
                                            <td class="center"><a onclick="return confirm('Bạn có chắc xóa danh mục này không?')" href="{{route('admin.category.delete', $item->ca_id)}}" class="active styling-edit"ui-toggle-class=""><i
                                                        class="fa fa-times text-danger text"></i> Delete</a></td>
                                            <td class="center"><a href="{{ route('admin.category.edit', $item->ca_id) }}" class="active styling-edit"ui-toggle-class=""><i
                                                        class="fa fa-pencil-square-o text-success text-active"></i> Edit</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
@endsection
