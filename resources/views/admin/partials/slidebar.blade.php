<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <div class="d-flex sidebar-profile">
                <div class="sidebar-profile-image">
                    <img src="{{asset('Admin/template/images/112.jpg')}}" alt="image">
                    <span class="sidebar-status-indicator"></span>
                </div>
                <div class="sidebar-profile-name">
                    <p class="sidebar-name">
                        CỬA HÀNG NỘI THẤT
                    </p>
                    <p class="sidebar-designation">
                        Welcome
                    </p>
                </div>
            </div>
            <div class="nav-search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Type to search..." aria-label="search" aria-describedby="search">
                    <div class="input-group-append">
                  <span class="input-group-text" id="search">
                    <i class="typcn typcn-zoom"></i>
                  </span>
                    </div>
                </div>
            </div>
            <p class="sidebar-menu-title">Dash menu</p>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/admin/home">
                <i class="typcn typcn-device-desktop menu-icon"></i>
                <span class="menu-title">Dashboard <span class="badge badge-primary ml-3">New</span></span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#theloai" aria-expanded="false" aria-controls="theloai">
                <i class="typcn typcn-briefcase menu-icon"></i>
                <span class="menu-title"> Thể loại</span>
                <i class="typcn typcn-chevron-right menu-arrow"></i>
            </a>
            <div class="collapse" id="theloai">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{route('admin.category.index')}}">Danh sách</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('admin.category.create')}}">Thêm mới</a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#sanphams" aria-expanded="false" aria-controls="sanphams">
                <i class="typcn typcn-film menu-icon"></i>
                <span class="menu-title">Sản phẩm</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="sanphams">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{route('admin.sanpham.index')}}">Danh sách</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('admin.sanpham.create')}}">Thêm mới</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#error" aria-expanded="false" aria-controls="charts">
                <i class="typcn typcn-globe-outline menu-icon"></i>
                <span class="menu-title">Nhà cung cấp</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="error">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{route('admin.NCC.index')}}">Danh sách</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('admin.NCC.create')}}">Thêm mới</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#icons" aria-expanded="false" aria-controls="icons">
                <i class="typcn typcn-compass menu-icon"></i>
                <span class="menu-title">Người dùng</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="icons">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{route('admin.user.index')}}">Danh sách</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('admin.user.create')}}">Thêm mới</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#error" aria-expanded="false" aria-controls="error">
                <i class="typcn typcn-globe-outline menu-icon"></i>
                <span class="menu-title">Đơn hàng</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="error">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{route('admin.order.index')}}"> Danh sách </a></li>
                    </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.thong-ke') }}">
                <i class="typcn typcn-globe-outline menu-icon"></i>
                <span class="menu-title">Thống kê</span>
{{--                <i class="menu-arrow"></i>--}}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.doanh-thu') }}">
                <i class="typcn typcn-globe-outline menu-icon"></i>
                <span class="menu-title">Doanh Thu</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="pages/documentation/documentation.html">
                <i class="typcn typcn-document-text menu-icon"></i>
                <span class="menu-title">Documentation</span>
            </a>
        </li>
    </ul>
</nav>
<!-- partial -->
