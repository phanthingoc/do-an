@extends('admin.layout.admin')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold">CỬA HÀNG NỘI THẤT</h3>

            </div>

        </div><br>
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h4> Sửa thông tin nội thất</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="/admin/home">Dashboard </a></li>
                                    <li>/</li>
                                    <li><a href="/admin/book">Nội thất</a></li>
                                    <li>/</li>
                                    <li class="active"> Sửa</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Sửa </strong>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('sanpham.update', $sanpham->b_id) }}" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="admin_view" value="true">
                                    {{ method_field('PUT') }}
                                    <div class="form-group">
                                        <label>Tên nội thất</label>
                                        <input class="form-control" name="b_name" placeholder="Nhập tên nội thất" value="{{ $sanpham->b_name }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="image">Image</label>
                                        <input type="file" class="form-control" id="image" name="b_image" value="{{ $sanpham->b_image }}"/>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Mô tả</label>
                                        <textarea name="mota" id="demo" class="form-control ckeditor" rows="5">{{ $sanpham->mota }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Thể loại</label>
                                        <select class="form-control" name="category_id" id="category_id">
                                            @foreach($category as $item)
                                                <option value="{{ $item->ca_id }}">{{ $item->ca_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Nhà cung cấp</label>
                                        <select class="form-control" name="ncc_id" id="category_id">
                                            @foreach($NCC as $item)
                                                <option value="{{ $item->ncc_id }}">{{ $item->ncc_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Giá</label>
                                        <input class="form-control" name="gia" placeholder="Nhập giá" value="{{ $sanpham->gia }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Số lượng</label>
                                        <input class="form-control" name="soluong" placeholder="Nhập số lượng" value="{{ $sanpham->soluong }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Sale %</label>
                                        <input class="form-control" name="sale" placeholder="Nhập % giảm giá" value="{{ $sanpham->sale }}"/>
                                    </div>

                                    <button type="submit" class="btn btn-outline-success">Sửa</button>
                                    <button type="reset" class="btn btn-outline-info">Làm mới </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

@endsection
