@extends('admin.layout.admin')
@section('title')
    <title> Trang chủ </title>
@endsection
@section('js')
    <script src="{{asset('Admin/js/sweetalert2@9.js')}}"></script>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold">CỬA HÀNG NỘI THẤT</h3>
            </div>
        </div><br>
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h4> Danh sách nội thất</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="/admin/home">Dashboard </a></li>
                                    <li>/</li>
                                    <li><a href="/admin/sanpham"> Nội thất</a></li>
                                    <li>/</li>
                                    <li class="active"> Danh sách</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Danh sách</strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Tên</th>
                                        <th>Ảnh</th>
                                        <th>Mô tả</th>
                                        <th>Thể loại</th>
                                        <th>Nhà cung cấp</th>
                                        <th>Giá</th>
                                        <th>Số lượng</th>
                                        <th>Delete</th>
                                        <th>Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sanpham as $item)
                                        <tr class="even gradeC">
                                            <td >{{$item['b_id']}}</td>
                                            <td style="width: 10%;">{{$item['b_name']}}</td>
                                            <td style="text-align: center; vertical-align: middle; width: 10%;">
                                                @if($item->b_image != '')
                                                    <img onclick="MymodalImage(this);" alt="{{ $item->b_image }}" src="{{asset('upload/sanpham/'.$item->b_image)}}" style="cursor: zoom-in;" width="20" height="20"/>
                                                @else
                                                    <img onclick="MymodalImage(this);" alt="{{ $item->b_image }}" src="{{asset('upload/sanpham/sach.jpg')}}" style="cursor: zoom-in;" width="60"/>
                                            @endif
                                            <td>{{$item['mota']}}</td>
                                            <td>{{$item['category_id']}}</td>
                                            <td>{{$item['ncc_id']}}</td>
                                            <td>{{ number_format($item['gia']) }}</td>
                                            <td>{{$item['soluong']}}</td>
                                            <td class="center"><a onclick="return confirm('Bạn có chắc xóa sách này không?')"  href="{{route('admin.sanpham.delete', $item->b_id)}}" class="btn btn-danger action_delete"><i
                                                        class="fa fa-times text-danger text"></i> Delete</a></td>
                                            <td class="center"><a href="{{ route('admin.sanpham.edit', $item->b_id) }}" class="btn btn-warning"><i
                                                        class="fa fa-pencil-square-o text-success text-active"></i> Edit</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
        <div id="myModal" class="modal" >
            <span class="close">&times;</span>
            <img class="modal-content" id="img01">
            <div id="caption"></div>
        </div>
        <script>
            function MymodalImage(e)
            {
                // Get the modal
                var modal = document.getElementById('myModal');
                // Get the image and insert it inside the modal - use its "alt" text as a caption
                var modalImg = document.getElementById("img01");
                var captionText = document.getElementById("caption");
                modal.style.display = "block";
                modalImg.src = e.src;
                captionText.innerHTML = e.alt;
                // Get the <span> element that closes the modal
                var span = document.getElementsByClassName("close")[0];
                // When the user clicks on <span> (x), close the modal
                span.onclick = function() {
                    modal.style.display = "none";
                }
            }
        </script>

@endsection
