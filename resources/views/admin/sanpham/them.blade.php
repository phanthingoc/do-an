@extends('admin.layout.admin')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold">CỬA HÀNG NỘI THẤT</h3>
            </div>
        </div><br>
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h4> Thêm sản phẩm</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="/admin/home">Dashboard </a></li>
                                    <li>/</li>
                                    <li><a href="/admin/book">Nội thất</a></li>
                                    <li>/</li>
                                    <li class="active"> Thêm</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Thêm</strong>
                            </div>
                            <div class="card-body">
                                <div class="col-md-12">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                                <form action="{{route('sanpham.store')}}" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="admin_view" value="true">
                                    <div class="form-group">
                                        <label>Tên</label>
                                        <input class="form-control" name="b_name" placeholder="Nhập tên nội thất"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="image">Ảnh</label>
                                        <input type="file" class="form-control" id="b_image" name="b_image"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Mô tả</label>
                                        <textarea name="mota" id="demo" class="form-control ckeditor" rows="2"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Thể loại</label>
                                        <select class="form-control" name="category_id" id="category_id">
                                            @foreach($category as $item)
                                                <option value="{{ $item->ca_id }}">{{ $item->ca_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Nhà cung cấp</label>
                                        <select class="form-control" name="ncc_id" id="category_id">
                                            @foreach($NCC as $item)
                                                <option value="{{ $item->ncc_id }}">{{ $item->ncc_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Giá</label>
                                        <input class="form-control" name="gia" placeholder="Nhập giá"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Số lượng</label>
                                        <input class="form-control" name="soluong" placeholder="Nhập số lượng"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Sale %</label>
                                        <input class="form-control" name="sale" placeholder="Nhập % giảm giá" />
                                    </div>

                                    <button type="submit" class="btn btn-warning">Thêm</button>
                                    <button type="reset" class="btn btn-info">Làm mới</button>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

    </div>
    <!-- content-wrapper ends -->

@endsection
