@extends('admin.layout.admin')
@section('title')
    <title> Thống kê </title>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold">CỬA HÀNG NỘI THẤT</h3>
            </div>
        </div><br>
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h4>Thống kê</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="/admin/home">Dashboard </a></li>
                                    <li>/</li>
                                    <li><a href="/admin/NCC"> Thống kê</a></li>
                                    <li>/</li>
                                    <li class="active"> Danh sách</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Thống kê</strong>
                            </div>
                            <div class="card-body">
                                <h4>Sản phẩm bán chạy nhất là: {{ $b_name }}  ({{ $so_luong_max }} sản phẩm) </h4>
                                <h4>Số lượng tồn kho là: {{ $count_ton_kho }} sản phẩm</h4>

                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
        <table class="table table-striped table-bordered">
            <thead style="">
            </thead>
            <tbody>
            <tr>
                <td style="text-align: center"><b>Doanh thu tháng này</b> ({{$currentYear}}-{{$currentMonth}})</td>
                <td style="text-align: center">{{number_format($sum)}} VND</td>
                <td style="text-align: center"><b>Tổng số đơn hàng đã bán tháng này</b> ({{$currentYear}}-{{$currentMonth}})</td>
                <td style="text-align: center">{{number_format($count)}}</td>
                <td style="text-align: center"><a href="{{URL::to('/chi-tiet-doanh-thu')}}/{{$currentMonth}}">Chi tiết</a></td>
            </tr>
            </tbody>
        </table>
        <div class="panel-heading">
            Doanh thu năm
        </div>
        <table class="table table-striped table-bordered">
            <thead style="">
            </thead>
            <tbody>
            <tr>
                <th style="text-align: center">Doanh thu năm</th>
                <th style="text-align: center">Tổng doanh thu</th>
            </tr>
            @foreach ($historyY as $item)
                <tr>
                    <td style="text-align: center">{{$item->nam}}</td>
                    <td style="text-align: center">{{number_format($item->tong_doanh_thu)}} VND</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
