
@extends('admin.layout.admin')

@section('title')
    <title> Trang chủ </title>
@endsection
@section('css')
    <link href="{{asset('Admin/main.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="app-main__inner">          
    <div class="row pt-lg-5">
        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content bg-midnight-bloom">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Tổng đơn hàng</div>
                        <div class="widget-subheading">Số đơn bán được</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span>{{ $tong_don }}</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content bg-arielle-smile">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Tổng tiền</div>
                        <div class="widget-subheading">Doanh thu bán được</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span>{{ number_format($tong_tien) }} VND</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content bg-grow-early">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Khách hàng</div>
                        <div class="widget-subheading">Tổng khách hàng</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span>{{ $tong_khach_hang }}</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

