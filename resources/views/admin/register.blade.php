<!DOCTYPE html>
<head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Visitors Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- bootstrap-css -->
    <link rel="stylesheet" href="{{asset('Admin/web/css/bootstrap.min.css')}}" >
    <!-- //bootstrap-css -->
    <!-- Custom CSS -->
    <link href="{{asset('Admin/web/css/style.css')}}" rel='stylesheet' type='text/css' />
    <link href="{{asset('Admin/web/css/style-responsive.css')}}" rel="stylesheet"/>
    <!-- font CSS -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- font-awesome icons -->
    <link rel="stylesheet" href="{{asset('Admin/web/css/font.css')}}" type="text/css"/>
    <link href="{{asset('Admin/web/css/font-awesome.css')}}" rel="stylesheet">
    <!-- //font-awesome icons -->
    <script src="{{asset('Admin/web/js/jquery2.0.3.min.js')}}"></script>
</head>
<body>
  <div class="log-w3">
    <div class="w3layouts-main">
    {{-- <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
          <div class="content-wrapper d-flex align-items-center auth px-0">
            <div class="row w-100 mx-0">
              <div class="col-lg-4 mx-auto">
                <div class="auth-form-light text-left py-5 px-4 px-sm-5"> --}}
                
                  <h4>Đăng ký?</h4>
                  <h6>Đăng ký tài khoản admin</h6>
                  <form class="pt-3" action="{{route('admin.registerPost')}}" method="post">
                    @csrf
                    <div class="form-group">
                      <span>Tên người dùng</span>
                      <input type="text" name="name_user" class="form-control form-control-lg" id="exampleInputUsername1" placeholder="Username">
                    </div>
                    <div class="form-group">
                      <span>Email</span>
                      <input type="email" name="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Email">
                    </div>
                    <div class="form-group">
                      <span>Mật khẩu</span>

                      <input type="password" name="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <div class="form-group">
                      <span>Số điện thoại</span>

                        <input type="number" name="phone" class="form-control form-control-lg" placeholder="Phone">
                      </div>
                      <input type="hidden" name="role" class="form-control form-control-lg" value="User">
                    <div class="mt-3">
                      <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit">SIGN UP</button>
                    </div>
                    <div class="text-center mt-4 font-weight-light">
                      Already have an account? <a href="{{ route('admin.login') }}" class="text-primary">Login</a>
                    </div>
                  </form>
                {{-- </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
      </div> --}}
    </div>
  </div>
<script src="{{'Admin/web/js/bootstrap.js'}}"></script>
<script src="{{'Admin/web/js/jquery.dcjqaccordion.2.7.js'}}"></script>
<script src="{{'Admin/web/js/scripts.js'}}"></script>
<script src="{{'Admin/web/js/jquery.slimscroll.js'}}"></script>
<script src="{{'Admin/web/js/jquery.nicescroll.js'}}"></script>
<script src="{{'Admin/web/js/jquery.scrollTo.js'}}"></script>
</body>

