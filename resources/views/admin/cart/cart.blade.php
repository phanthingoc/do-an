@extends('admin.layout.admin')
@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Đơn hàng</h4>
                <div class="table-responsive pt-3">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>
                               Tên người đặt
                            </th>
                            <th>
                                Tổng giá tiền
                            </th>
                            <th>
                                Tình trạng
                            </th>
                            <th>
                                Hiển thị
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($all_order as $key => $order)
                        <tr>
                            <td>{{$order->ct_name}} </td>
                            <td>{{number_format( $order->tongtien)}} đ</td>
                            <td>{{$order->trangthai}}</td>
                            <td>
                                @if($order->trangthai == 'Đang chờ xử lý' )
                                <a href="{{ route('admin.order.complete', $order->order_id) }}" class="btn btn-outline-info">Duyệt đơn</a>
                                @endif
                                <a href="{{ route('admin.order.view', $order->order_id) }}" class="btn btn-outline-info"><i
                                        class="far fa-edit"></i> View</a>
                                        
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
