@extends('admin.layout.admin')
@section('title')
    <title> Trang chủ </title>
@endsection

@section('content')
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Thông tin người mua</h4>
                <div class="table-responsive pt-3">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>
                                Tên người đặt
                            </th>
                            <th>
                                Số điện thoại
                            </th>

                        </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>{{$orderbyId->ct_name}} </td>
                                <td>{{$orderbyId-> sdt}} </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-body">
                <h4 class="card-title">Thông tin vận chuyển</h4>
                <div class="table-responsive pt-3">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>
                                Tên người nhận hàng
                            </th>
                            <th>
                                Địa chỉ
                            </th>
                            <th>
                                Số điện thoại
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>{{$orderbyId-> ship_name}} </td>
                                <td>{{$orderbyId-> ship_address}} </td>
                                <td>{{$orderbyId-> ship_phone}}</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-body">
                <h4 class="card-title">Liệt kê chi tiết đơn hàng</h4>
                <div class="table-responsive pt-3">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>
                                Tên sản phẩm
                            </th>
                            <th>
                                Số lượng
                            </th>
                            <th>
                               Giá sản phẩm
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($listOrder as $order)
                            <tr>
                                <td>{{$order->b_name}} </td>
                                <td>{{$order->soluong}} </td>
                                <td>{{ number_format($order->gia) }}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="2">Tổng tiền</td>
                                <td>{{ number_format($orderbyId->tongtien) }}</td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
