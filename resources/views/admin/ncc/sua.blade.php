@extends('admin.layout.admin')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold">CỬA HÀNG NỘI THẤT</h3>

            </div>

        </div><br>
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h4> Sửa thông tin nhà cung cấp</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="/admin/home">Dashboard </a></li>
                                    <li>/</li>
                                    <li><a href="/admin/NCC">Nhà cung cấp</a></li>
                                    <li>/</li>
                                    <li class="active"> Sửa</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Sửa </strong>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('ncc.update', $NCC->ncc_id) }}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="admin_view" value="true">
                                    {{ method_field('PUT') }}
                                    <div class="form-group">
                                        <label>Tên nhà cung cấp</label>
                                        <input class="form-control" name="ncc_name" placeholder="Nhập tên thể loại" value="{{ $NCC->ncc_name }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Số điện thoại</label>
                                        <input class="form-control" name="sdt" placeholder="Nhập số điện thoại" value="{{ $NCC->sdt }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" name="email" placeholder="Nhập email" value="{{ $NCC->email }}"/>
                                    </div>

                                    <button type="submit" class="btn btn-outline-success">Sửa</button>
                                    <button type="reset" class="btn btn-outline-info">Làm mới </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

@endsection
