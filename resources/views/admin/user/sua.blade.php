@extends('admin.layout.admin')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="mb-0 font-weight-bold">CỬA HÀNG NỘI THẤT</h3>

            </div>

        </div><br>
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h4> Sửa thông tin user</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="/admin/home">Dashboard </a></li>
                                    <li>/</li>
                                    <li><a href="/admin/User"> User</a></li>
                                    <li>/</li>
                                    <li class="active"> Sửa</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Sửa </strong>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('user.update', $User->id_user) }}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="admin_view" value="true">
                                    {{ method_field('PUT') }}
                                    <div class="form-group">
                                        <label>Tên </label>
                                        <input class="form-control" name="name_user" placeholder="Nhập tên thể loại" value="{{ $User->name_user }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" name="email" placeholder="Nhập email" value="{{ $User->email }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" disabled class="form-control" name="password" placeholder="Nhập mật khẩu" value="{{ $User->password }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Số điện thoại</label>
                                        <input class="form-control" name="phone" placeholder="Nhập số điện thoại" value="{{ $User->phone }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Địa chỉ</label>
                                        <input class="form-control" name="address" placeholder="Nhập địa chỉ" value="{{ $User->address }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Giới tính</label>
                                        <input class="form-control" name="gender" placeholder="Nhập giới tính" value="{{ $User->gender }}"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Quyền</label>
                                        <select class="form-control" name="role" value="{{ $User->role }}">
                                            <option>Admin</option>
                                            <option>User</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-outline-success">Sửa</button>
                                    <button type="reset" class="btn btn-outline-info">Làm mới </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

@endsection
